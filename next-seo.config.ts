export default {
    openGraph: {
        type: 'website',
        locale: 'en_IE',
        url: 'https://www.naavikmusik.com/',
        site_name: 'NAAVIK MUSIK'
    },
    twitter: {
        handle: '@naavikmusik',
        site: '@naavikmusik',
        cardType: 'summary_large_image'
    }
};
