import Divider from '../components/divider'
import Drawer from '../components/drawer'
import FormHeader from '../components/form_header'
import Navbar from '../components/navbar'
import UploadImageForm from '../components/upload_image_form'
import useDrawer from '../hooks/use_drawer'
import UploadedImages from '../components/uploaded_images'
import Footer from '../components/footer'
import { NextPage } from 'next'
import { NextSeo } from 'next-seo'
import withAuth from '../components/with_auth'

const UploadImage: NextPage<any> = () => {
    const { open, toggleDrawer } = useDrawer()

    const title = "Upload Image - NAAVIK Musik"
    const description = "Image uploading portal for NAAVIK Musik official site available only to authorized users."
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/upload-image"

    return (
        <main>
            <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description }}
            />
            <Navbar toggleDrawer={toggleDrawer} />
            <FormHeader title="Upload Image" />
            <UploadImageForm />
            <Divider />
            <UploadedImages />
            <Divider />
            <Footer />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
        </main>
    )
}

export default withAuth(UploadImage, "/upload-image")
