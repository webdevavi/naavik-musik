import { verifyIdToken } from '../../firebase/admin'

const validate = async (token: string) => await verifyIdToken(token)

export default async (req: any, res: any) => {
    try {
        const { token } = JSON.parse(req.headers.authorization || '{}')
        console.log(token)
        if (!token) {
            return res.status(403).send({
                errorCode: 403,
                message: 'Auth token missing.',
            })
        }
        return res.status(200).send(await validate(token))
    } catch (err) {
        return res.status(200).send(undefined)
    }
}