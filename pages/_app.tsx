import { DefaultSeo } from 'next-seo'
import { ResponsiveBreakpointsProvider } from "simple-responsive-react"
import '../styles/index.css'
import '../styles/form.css'
import SEO from '../next-seo.config'


const MyApp = ({ Component, pageProps }) => (
    <ResponsiveBreakpointsProvider>
        <DefaultSeo {...SEO} />
        <Component {...pageProps} />
    </ResponsiveBreakpointsProvider>
)

export default MyApp
