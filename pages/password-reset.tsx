import PasswordReset from '../components/password_reset'
import Navbar from '../components/navbar'
import useDrawer from '../hooks/use_drawer'
import Drawer from '../components/drawer'
import React from 'react'
import FormHeader from '../components/form_header'
import { NextSeo } from 'next-seo'


const Auth = () => {
    const { open, toggleDrawer } = useDrawer()

    const title = "Password Reset - NAAVIK Musik"
    const description = "Password resetting portal for NAAVIK Musik official site available only to authorized users."
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/password-reset"

    return (
        <main>
            <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description }}
            />
            <Navbar toggleDrawer={toggleDrawer} />
            <FormHeader title="Password Reset" />
            <PasswordReset />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
        </main>
    )
}

export default Auth