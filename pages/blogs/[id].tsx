import { useRouter } from "next/router"
import React, { useEffect, useState } from "react"
import Navbar from '../../components/navbar'
import useDrawer from '../../hooks/use_drawer'
import Drawer from '../../components/drawer'
import BlogContainer from "../../components/blog_container"
import Footer from "../../components/footer"
import { NextPage } from "next"
import { getBlog } from "../../utils/blog"
import Toast from "../../components/toast"
import { NextSeo } from "next-seo"
import { Blog } from "../../models/blog"

const BlogPage: NextPage<any> = ({
    blogJSON, error
}) => {
    const router = useRouter()
    const { open, toggleDrawer } = useDrawer()
    const [openError, setOpenError] = useState(error != null)
    const blog = blogJSON && Blog.fromJSON(blogJSON)
    useEffect(() => {
        if (!blogJSON) router.replace('/404')
    })

    const title = blog?.title
    const description = blog?.description
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/blogs/" + blog?.id
    const images = [{ url: blog?.thumbnail?.url }]

    return (
        <main>
            {blog && <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description, images }}
            />}
            <Navbar toggleDrawer={toggleDrawer} />
            <BlogContainer blog={blog} />

            <Footer />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
            <Toast
                open={openError}
                severity="error"
                onClose={() => setOpenError(false)}
                message={error}
            />
        </main>
    )
}

BlogPage.getInitialProps = async (ctx) => {
    const { id } = ctx.query

    try {
        const blog = await getBlog(id as string)
        return {
            blogJSON: blog.toJSON()
        }
    } catch ({ message }) {
        return {
            error: message
        }
    }
}

export default BlogPage
