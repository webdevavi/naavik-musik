import { useRouter } from "next/router"
import React, { useEffect, useState } from "react"
import Navbar from '../../../components/navbar'
import useDrawer from '../../../hooks/use_drawer'
import Drawer from '../../../components/drawer'
import Footer from "../../../components/footer"
import { NextPage } from "next"
import { getBlog } from "../../../utils/blog"
import Toast from "../../../components/toast"
import { NextSeo } from "next-seo"
import { Blog } from "../../../models/blog"
import CreateBlogForm from "../../../components/create_blog_form"
import FormHeader from "../../../components/form_header"
import Divider from "../../../components/divider"
import withAuth from "../../../components/with_auth"

const BlogEditPage: NextPage<any> = ({
    blogJSON, error
}) => {
    const router = useRouter()
    const { open, toggleDrawer } = useDrawer()
    const [openError, setOpenError] = useState(error != null)
    const blog = blogJSON && Blog.fromJSON(blogJSON)
    useEffect(() => {
        if (!blogJSON) router.replace('/404')
    })

    const title = "Edit - " + blog?.title
    const description = blog?.description
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/blogs/edit/" + blog?.id
    const images = [{ url: blog?.thumbnail?.url }]

    return (
        <main>
            {blog && <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description, images }}
            />}
            <Navbar toggleDrawer={toggleDrawer} />
            <FormHeader title="Edit Blog" />
            <CreateBlogForm editing blog={blog} />
            <Divider />
            <Footer />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
            <Toast
                open={openError}
                severity="error"
                onClose={() => setOpenError(false)}
                message={error}
            />
        </main>
    )
}

export default withAuth(BlogEditPage, "/blogs", async (ctx) => {
    const { id } = ctx.query

    try {
        const blog = await getBlog(id as string)
        return {
            blogJSON: blog.toJSON(),
        }
    } catch ({ message }) {
        return {
            error: message,
        }
    }
})
