import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import Divider from '../../components/divider'
import Drawer from '../../components/drawer'
import FormHeader from '../../components/form_header'
import Navbar from '../../components/navbar'
import useDrawer from '../../hooks/use_drawer'
import Footer from '../../components/footer'
import { NextPage } from 'next'
import CreateBlogForm from '../../components/create_blog_form'
import { NextSeo } from 'next-seo'
import withAuth from '../../components/with_auth'

const CreateBlog: NextPage<any> = () => {
    const { open, toggleDrawer } = useDrawer()
    const title = "Create Blog - NAAVIK Musik"
    const description = "Blog creating portal for NAAVIK Musik official site available only to authorized users."
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/blogs/create"

    return (
        <main>
            <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description }}
            />
            <Navbar toggleDrawer={toggleDrawer} />
            <FormHeader title="Create Blog" />
            <CreateBlogForm />
            <Divider />
            <Footer />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
        </main>
    )
}

export default withAuth(CreateBlog, "/blogs/create")
