import { NextPage } from "next"
import { NextSeo } from "next-seo"
import React from "react"
import Drawer from "../../components/drawer"
import Footer from "../../components/footer"
import Navbar from "../../components/navbar"
import useDrawer from "../../hooks/use_drawer"
import Blog from "../../models/blog"
import { getAllBlogs } from "../../utils/blog"
import SectionHeader from '../../components/section_header'
import BlogsContainer from '../../components/blogs_container'
import Divider from "../../components/divider"

const BlogsPage: NextPage<any> = ({ blogsJSON, error }) => {
    const blogs = blogsJSON?.map(
        blogJSON => Blog.fromJSON(blogJSON)
    )
    const { open, toggleDrawer } = useDrawer()

    const title = "Blogs - NAAVIK Musik"
    const description = "All blogs by NAAVIK Musik team."
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/blogs"
    const images = [{ url: '/logo-dark.png' }, { url: '/logo-dark.png' }]


    return (
        <main>
            <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description, images }}
            />
            <Navbar toggleDrawer={toggleDrawer} />
            <SectionHeader title="NAAVIK Musik's  Blogs" />
            <BlogsContainer blogs={blogs} />
            <Divider />
            <Footer />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
        </main>
    )
}

BlogsPage.getInitialProps = async () => {

    try {
        const blogs = await getAllBlogs()
        const blogsJSON = blogs?.map(
            blog => blog?.toJSON()
        )
        return { blogsJSON }
    } catch ({ message }) {
        return { error: message }
    }

}

export default BlogsPage