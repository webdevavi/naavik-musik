import Head from 'next/head'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import Spotify from '../services/spotify'
import Navbar from '../components/navbar'
import Hero from '../components/hero'
import Artist from '../models/artist'
import Album from '../models/album'
import AlbumsSection from '../components/albums_section'
import TracksSection from '../components/tracks_section'
import Track from '../models/track'
import React from 'react'
import AboutSection from '../components/about_section'
import VisionSection from '../components/vision_section'
import MissionSection from '../components/mission_section'
import Footer from '../components/footer'
import { NextSeo } from 'next-seo'
import useDrawer from '../hooks/use_drawer'
import Drawer from '../components/drawer'

const Home: React.FC<InferGetServerSidePropsType<typeof getServerSideProps>> = ({ artistJSON, albumsJSON, tracksJSON, error }) => {
    const artist = artistJSON && Artist.fromJSON(artistJSON)
    const albums = albumsJSON?.map((albumJSON: any) => Album.fromJSON(albumJSON))
    const tracks = tracksJSON?.map((trackJSON: any) => Track.fromJSON(trackJSON))
    const { open, toggleDrawer } = useDrawer()
    const title = "NAAVIK Musik"
    const description = "The official website of NAAVIK MUSIK"
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN
    const images = [{ url: '/logo-dark.png' }, { url: '/logo-dark.png' }]

    return (
        <main>
            <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description, images }}
            />
            <Head>
                <title>Official NAAVIK MUSIK Website</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar toggleDrawer={toggleDrawer} />
            <Hero artist={artist} />
            <AlbumsSection albums={albums} />
            <TracksSection tracks={tracks} />
            <AboutSection />
            <VisionSection />
            <MissionSection />
            <Footer />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
        </main>
    )
}

export const getServerSideProps: GetServerSideProps = async () => {

    try { // Requesting for an access token from Spotify
        const token = await Spotify.getAccessToken(

            // by passing Client's Id
            process.env.SPOTIFY_CLIENT_ID,

            // and Client's secret
            process.env.SPOTIFY_CLIENT_SECRET
        )

        // Initialising the Spotify class from [services/spotify]
        // passing the access token we got from [getAccessToken]
        const spotify = new Spotify(token)

        // Requesting for the artist's profile from Spotify
        const artistJSON = await spotify.getArtist(process.env.SPOTIFY_ARTIST_ID)

        // Requesting for the artist's albums from Spotify
        const albumsJSON = await spotify.getArtistAlbums(process.env.SPOTIFY_ARTIST_ID)

        // Requesting for the artist's top tracks in ["IN" India] from Spotify
        const tracksJSON = await spotify.getArtistTopTracks(process.env.SPOTIFY_ARTIST_ID, 'IN')

        // Passing the artist's profile, albums and tracks to
        // the Home Component/Page if no error occurs
        return {
            props: {
                artistJSON,
                albumsJSON,
                tracksJSON
            }
        }

        // Catching errors if any occur
    } catch (err) {

        // Passing error message to the Home Component/Page if
        // any error occurs
        return {
            props: {
                error: err.message
            }
        }
    }

}

export default Home
