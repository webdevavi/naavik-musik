import FirebaseAuth from '../components/firebase_auth'
import Navbar from '../components/navbar'
import useDrawer from '../hooks/use_drawer'
import Drawer from '../components/drawer'
import React from 'react'
import FormHeader from '../components/form_header'
import { NextSeo } from 'next-seo'
import { useRouter } from 'next/router'


const Auth = () => {
    const { open, toggleDrawer } = useDrawer()

    const router = useRouter()
    const { redirect } = router.query

    const title = "Login- NAAVIK Musik"
    const description = "Login portal for NAAVIK Musik official site available only to authorized users."
    const canonical = "https://" + process.env.NEXT_PUBLIC_DOMAIN + "/login"

    return (
        <main>
            <NextSeo
                title={title}
                description={description}
                canonical={canonical}
                openGraph={{ title, description }}
            />
            <Navbar toggleDrawer={toggleDrawer} />
            <FormHeader title="Login as Admin" />
            <FirebaseAuth redirect={redirect as string} />
            <Drawer open={open} toggleDrawer={toggleDrawer} />
        </main>
    )
}

export default Auth