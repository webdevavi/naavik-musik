export interface IImage {
    path?: string
    url?: string
    name?: string
}

export class Image implements IImage {
    path?: string
    url?: string
    name?: string

    constructor({
        path, url, name
    }: IImage) {
        this.path = path
        this.url = url
        this.name = name
    }

    public static fromJSON({
        path,
        url,
        name
    }: {
        path?: string,
        url?: string,
        name?: string
    }
    ) {
        return new Image({ path, url, name })

    }

    public toJSON = () => ({
        "path": this.path || null,
        "url": this.url || null,
        "name": this.name || null
    })
}

export default Image