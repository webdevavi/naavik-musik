export interface IMessage {
    name?: string
    email?: string
    message?: string
}

export class Message implements IMessage {
    name?: string
    email?: string
    message?: string

    constructor({
        name, email, message
    }: IMessage) {
        this.name = name
        this.email = email
        this.message = message
    }

    public static fromJSON({
        name,
        email,
        message
    }: {
        name?: string,
        email?: string,
        message?: string
    }
    ) {
        return new Message({ name, email, message })

    }

    public toJSON = () => ({
        "name": this.name || null,
        "email": this.email || null,
        "message": this.message || null
    })
}

export default Message