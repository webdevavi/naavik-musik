import Album from "./album";
import Artist from "./artist";

export interface ITrack {
    // Unique Id of the track assigned by Spotify
    id?: string

    // Name of the track as on Spotify
    name?: string

    // Artists in the track
    artists?: Artist[]

    // A link to the track on Spotify
    link?: string

    // A preview URL to the track on Spotify
    previewURL?: string

    // The album which the track is a part of
    album?: Album

    // URI of the track given by Spotify
    uri?: string

    // If the track is explicit or not
    explicit?: boolean
}

export default class Track implements ITrack {
    id?: string
    name?: string
    artists?: Artist[]
    link?: string
    previewURL?: string
    album?: Album
    uri?: string
    explicit?: boolean

    constructor(
        {
            id,
            name,
            artists,
            link,
            previewURL,
            album,
            uri,
            explicit
        }: ITrack
    ) {
        this.id = id
        this.name = name
        this.artists = artists
        this.previewURL = previewURL
        this.link = link
        this.album = album
        this.uri = uri
        this.explicit = explicit
    }

    public static fromJSON = (
        {
            id,
            name,
            external_urls,
            album,
            artists,
            preview_url,
            uri,
            explicit
        }: {
            id: string
            name: string
            artists: {
                id: string
                name: string
                external_urls: {
                    spotify: string
                }
                images: {
                    url: string
                }[]
                uri: string
            }[]
            external_urls: {
                spotify: string
            }
            album: {
                id: string
                name: string
                artists: {
                    id: string
                    name: string
                    external_urls: {
                        spotify: string
                    }
                    images: {
                        url: string
                    }[]
                    uri: string
                }[]
                external_urls: {
                    spotify: string
                }
                total_tracks: number
                images: {
                    url: string
                }[]
                uri: string
            }
            preview_url: string
            uri: string
            explicit: boolean
        }
    ) => {
        return new Track({
            id,
            name,
            link: external_urls?.spotify,
            artists: artists?.map(
                artist => Artist.fromJSON(artist)
            ),
            album: Album.fromJSON(album),
            previewURL: preview_url,
            uri,
            explicit
        })
    }

    public toJSON = () => ({
        id: this.id,
        name: this.name,
        artists: this.artists?.map(artist => artist.toJSON()),
        album: this.album?.toJSON(),
        link: this.link,
        preview_url: this.previewURL,
        uri: this.uri,
        explicit: this.explicit
    })
}