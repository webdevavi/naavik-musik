import Artist from "./artist";

export interface IAlbum {
    // Unique Id of the album assigned by Spotify
    id?: string

    // Name of the album as on Spotify
    name?: string

    // Total no. of tracks in the album
    totalTracks?: number

    // Artists in the album
    artists?: Artist[]

    // A link to the album on Spotify
    link?: string

    // URL of an image of the album from Spotify
    imageURL?: string

    // URI of the album given by Spotify
    uri?: string
}

export default class Album implements IAlbum {
    id?: string
    name?: string
    totalTracks?: number
    artists?: Artist[]
    link?: string
    imageURL?: string
    uri?: string

    constructor(
        {
            id,
            name,
            totalTracks,
            artists,
            link,
            imageURL,
            uri
        }: IAlbum
    ) {
        this.id = id
        this.name = name
        this.totalTracks = totalTracks
        this.artists = artists
        this.link = link
        this.imageURL = imageURL
        this.uri = uri
    }

    public static fromJSON = (
        {
            id,
            name,
            external_urls,
            total_tracks,
            artists,
            images,
            uri
        }: {
            id: string
            name: string
            artists: {
                id: string
                name: string
                external_urls: {
                    spotify: string
                }
                images: {
                    url: string
                }[]
                uri: string
            }[]
            external_urls: {
                spotify: string
            }
            total_tracks: number
            images: {
                url: string
            }[]
            uri: string
        }
    ) => {
        return new Album({
            id,
            name,
            link: external_urls?.spotify,
            totalTracks: total_tracks,
            artists: artists?.map(
                artist => Artist.fromJSON(artist)
            ),
            imageURL: images && images[0]?.url,
            uri
        })
    }

    public toJSON = () => ({
        id: this.id,
        name: this.name,
        total_tracks: this.totalTracks,
        artists: this.artists?.map(artist => artist.toJSON()),
        link: this.link,
        image_url: this.imageURL,
        uri: this.uri
    })
}