export interface IArtist {
    // Unique id of the artist assigned by Spotify
    id?: string

    // Name of the artist as on Spotify
    name?: string

    // A link to the artist's profile on Spotify
    link?: string

    // URL of an image of the artist from Spotify profile
    imageURL?: string

    // URI of the artist given by Spotify
    uri?: string
}

export default class Artist implements IArtist {
    id?: string;
    name?: string;
    link?: string;
    imageURL?: string;
    uri: string;

    constructor(
        {
            id,
            name,
            link,
            imageURL,
            uri
        }: IArtist
    ) {
        this.id = id
        this.name = name
        this.link = link
        this.imageURL = imageURL
        this.uri = uri
    }

    public static fromJSON = (
        {
            id,
            name,
            external_urls,
            images,
            uri
        }: {
            id: string
            name: string
            external_urls: {
                spotify: string
            }
            images: {
                url: string
            }[]
            uri: string
        }
    ) => {
        return new Artist({
            id,
            name,
            link: external_urls?.spotify,
            imageURL: images && images[0]?.url,
            uri
        })
    }

    public toJSON = () => ({
        id: this.id,
        name: this.name,
        link: this.link,
        image_url: this.imageURL,
        uri: this.uri
    })

}