import firebase from "firebase";
import Image from "./image";

const { firestore } = firebase


export interface IBlog {
    id?: string
    title: string
    description: string
    createdAt?: Date
    modifiedAt?: Date
    content: string
    thumbnail: Image
}

export class Blog implements IBlog {
    id?: string
    title: string
    description: string
    createdAt?: Date
    modifiedAt?: Date
    content: string
    thumbnail: Image

    constructor(
        {
            id,
            title,
            description,
            createdAt,
            modifiedAt,
            content,
            thumbnail,
        }: IBlog
    ) {
        this.id = id
        this.title = title
        this.description = description
        this.createdAt = createdAt
        this.modifiedAt = modifiedAt
        this.content = content
        this.thumbnail = thumbnail
    }

    public static fromJSON({
        id,
        title,
        description,
        created_at,
        modified_at,
        content,
        thumbnail,
    }: {
        id?: string
        title: string
        description: string
        created_at: string
        modified_at: string
        content: string
        thumbnail: {
            path: string
            url: string
        }
    }) {
        return new Blog({
            id,
            title,
            description,
            createdAt: new Date(created_at),
            modifiedAt: modified_at ? new Date(modified_at) : null,
            content,
            thumbnail: Image.fromJSON(thumbnail),
        })
    }

    public static fromFirebase({
        title,
        description,
        created_at,
        modified_at,
        content,
        thumbnail,
    }: firebase.firestore.DocumentData,
        id: string
    ) {
        return new Blog({
            id,
            title,
            description,
            createdAt: created_at?.toDate(),
            modifiedAt: modified_at ? modified_at?.toDate() : null,
            content,
            thumbnail: Image.fromJSON(thumbnail),
        })
    }

    public toFirebase = (editing = false) => {
        const blog = {
            "title": this.title,
            "description": this.description,
            "content": this.content,
            "thumbnail": this.thumbnail.toJSON(),
        }
        if (editing) return {
            ...blog,
            "modified_at": firestore.FieldValue.serverTimestamp(),
        }
        else return {
            ...blog,
            "created_at": firestore.FieldValue.serverTimestamp(),
        }
    }

    public toJSON = () => ({
        "id": this.id,
        "title": this.title,
        "description": this.description,
        "created_at": this.createdAt?.toJSON() || null,
        "modified_at": this.modifiedAt?.toJSON() || null,
        "content": this.content,
        "thumbnail": this.thumbnail.toJSON(),
    })
}

export default Blog