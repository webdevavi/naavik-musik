import { Typography } from '@material-ui/core'
import styles from './paragraph.module.scss'

const Parapgraph = ({ children }) => {
    return (
        <Typography
            className={styles.paragraph}
            component="p"
        >
            {children}
        </Typography>
    )
}

export default Parapgraph
