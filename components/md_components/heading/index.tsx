import { Typography } from "@material-ui/core"
import styles from './heading.module.scss'

interface HeadingProps {
    as?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"
}

const Heading: React.FC<HeadingProps> = ({ as, children }) => {
    return (
        <Typography
            className={[styles.heading, styles[as]].join(" ")}
            component={as}
        >
            {children}
        </Typography>

    )
}

export default Heading