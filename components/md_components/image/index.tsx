import styles from './image.module.scss'

const Image = (props: any) => {
    return (
        <span className={styles.container}>
            <img
                {...props}
                className={styles.image}
            />
            {props.alt && <span className={styles.altText}>
                {props.alt}
            </span>}
        </span>
    )
}

export default Image