import {
    Paper,
    TableContainer,
    Table as MuiTable,
    TableHead,
    TableBody,
    TableRow,
    TableCell
} from '@material-ui/core'
import styles from './table.module.scss'

export const Table = ({ children }) => (
    <TableContainer component={Paper} className={styles.root}>
        <MuiTable className={styles.table}>
            {children}
        </MuiTable>
    </TableContainer>
)

export const THead = ({ children }) => (
    <TableHead className={styles.tHead}>
        {children}
    </TableHead>
)

export const TBody = ({ children }) => (
    <TableBody className={styles.tBody}>
        {children}
    </TableBody>
)

export const TRow = ({ children }) => (
    <TableRow className={styles.tRow}>
        {children}
    </TableRow>
)

export const TH = ({ children }) => (
    <th className={styles.th}>
        {children}
    </th>
)

export const TCell = ({ children }) => (
    <TableCell className={styles.tCell}>
        {children}
    </TableCell>
)