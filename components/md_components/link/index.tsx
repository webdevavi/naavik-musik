import { Link as ExtLink, Badge } from '@material-ui/core'
import { FaExternalLinkAlt } from 'react-icons/fa'

const Link = (props: any) => {
    return (
        <Badge
            badgeContent={
                <FaExternalLinkAlt size={10} />
            }
        >
            <ExtLink
                {...props}
            >
                {props?.children}
            </ExtLink>
        </Badge>
    )
}

export default Link