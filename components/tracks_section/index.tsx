import React, { FC } from 'react'
import { Container } from '@material-ui/core'
import Responsive from 'simple-responsive-react'
import Track from '../../models/track'
import SectionHeader from '../section_header'
import TrackContainer from '../track_container'
import styles from './tracks_section.module.scss'
import Divider from '../divider'

interface TracksSectionProps { tracks: Track[] }

const TracksSection: FC<TracksSectionProps> = ({ tracks }) => {
    return (
        <>
            <Divider />
            <Container maxWidth="md" id="music" className={styles.tracksSection}>
                <SectionHeader title="Top Tracks" />
                <Responsive type="above" screen="mobile">
                    <div className={styles.containerForAboveMobile}>
                        <div className={styles.tracksContainer}>
                            {
                                tracks?.map(
                                    (track, index) => index < 5 && <TrackContainer
                                        key={track?.id}
                                        track={track}
                                    />
                                )
                            }

                        </div>
                        <div className={styles.tracksContainer}>
                            {
                                tracks?.map(
                                    (track, index) => index > 4 && <TrackContainer
                                        key={track?.id}
                                        track={track}
                                    />
                                )
                            }

                        </div>
                    </div>
                </Responsive>
                <Responsive type="only" screen="mobile">
                    <div className={styles.tracksContainer}>
                        {
                            tracks?.map(
                                track => <TrackContainer
                                    key={track?.id}
                                    track={track}
                                />
                            )
                        }

                    </div>
                </Responsive>
            </Container>
        </>
    )
}

export default TracksSection