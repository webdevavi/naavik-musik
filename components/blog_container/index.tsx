import { Container, Divider } from '@material-ui/core'
import { Blog } from '../../models/blog'
import BlogContent from '../blog_content'
import BlogTitle from '../blog_title'
import RecentBlogs from '../recent_blogs'
import styles from './blog_container.module.scss'

interface BlogContainerProps {
    blog: Blog
}

const BlogContainer: React.FC<BlogContainerProps> = (
    { blog }
) => {

    return (
        <div className={styles.container}>
            <BlogTitle title={blog?.title} />
            <Container
                className={styles.body}
                maxWidth="md"
            >
                <div className={styles.content}>
                    <BlogContent blog={blog} />
                </div>
                <Divider variant="fullWidth" />
                <div className={styles.recentBlogs}>
                    <RecentBlogs />
                </div>
            </Container>
        </div>
    )
}



export default BlogContainer
