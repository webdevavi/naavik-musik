import {
    Card,
    CardHeader,
    CardContent,
    CardMedia,
    Container,
    Grid,
    Typography,
    Link, Button, Fab, Grow
} from "@material-ui/core";
import { useRouter } from "next/router";
import React from "react";
import { FaPlus } from "react-icons/fa";
import Blog from "../../models/blog";
import useAuth from "../../utils/auth/use_auth";
import BlogTimestamp from "../blog_timestamp";
import styles from "./blog_container.module.scss"

interface BlogsContainerProps {
    blogs: Blog[]
}

const BlogsContainer: React.FC<BlogsContainerProps> = (
    { blogs }
) => {
    const router = useRouter()
    const { token } = useAuth()
    return (
        <Container maxWidth="md">
            <Grid
                className={styles.container}
                container
            >
                {blogs?.map(
                    blog => (
                        <Link
                            key={blog?.id}
                            className={styles.root}
                            href={`/blogs/${blog?.id}`}
                        >
                            <Card
                                className={styles.card}
                            >
                                <CardHeader
                                    className={styles.header}
                                    title={blog?.title}
                                />
                                <CardMedia
                                    className={styles.media}
                                    image={blog?.thumbnail?.url}
                                />
                                <CardContent>
                                    <BlogTimestamp
                                        createdAt={blog?.createdAt}
                                        modifiedAt={blog?.modifiedAt}
                                    />
                                    <Typography
                                        className={styles.description}
                                        component="span"
                                    >
                                        {blog?.description}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Link>
                    )
                )}
            </Grid>

            {  token && (
                <Grow in={token != null} timeout={1000}>
                    <Fab
                        className={styles.addButton}
                        variant="extended"
                        onClick={() => router.push('/blogs/create')}
                    >
                        New Blog
                        <FaPlus className={styles.addIcon} />
                    </Fab>
                </Grow>
            )}
        </Container>
    )
}

export default BlogsContainer