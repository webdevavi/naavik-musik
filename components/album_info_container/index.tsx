import React from 'react'
import styles from './album_info_container.module.scss'
import Album from '../../models/album'
import { Button, Typography } from '@material-ui/core'
import Responsive from 'simple-responsive-react'
import { FaSpotify } from 'react-icons/fa'

interface AlbumInfoContainerProps {
    album: Album
}

const AlbumInfoContainer: React.FC<AlbumInfoContainerProps> = (
    { album }
) => {
    return (

        <div className={styles.albumInfo}>
            <Typography variant="h3" className={styles.albumName}>
                {album?.name}
            </Typography>
            <div className={styles.artistsName}>
                {album?.artists?.map(
                    artist => artist?.name
                ).join(", ")}
            </div>
            <div className={styles.totalTracks}>
                Total {album?.totalTracks?.toString()} {album?.totalTracks !== 1 ? 'tracks' : 'track'}
            </div>
            <Responsive
                type="only"
                screen="mobile"
            >
                <Button
                    className={styles.playButton}
                    size="medium"
                    startIcon={<FaSpotify color="#1DB954" />}
                    onClick={() => window.open(album?.link)}
                >
                    PLAY
            </Button>
            </Responsive>
            <Responsive
                type="above"
                screen="mobile"
            >
                <Button
                    className={styles.playButton}
                    size="large"
                    startIcon={<FaSpotify size={24} color="#1DB954" />}
                    onClick={() => window.open(album?.link)}
                >
                    PLAY
            </Button>
            </Responsive>
        </div>
    )
}

export default AlbumInfoContainer
