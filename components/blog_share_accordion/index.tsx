import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    ButtonGroup,
    Fade,
    Grow,
    IconButton,
    Typography
} from '@material-ui/core'
import { Share } from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import { FaClipboard } from 'react-icons/fa'
import {
    FacebookIcon,
    FacebookMessengerIcon,
    FacebookMessengerShareButton,
    FacebookShareButton,
    RedditIcon,
    RedditShareButton,
    TelegramIcon,
    TelegramShareButton,
    TwitterIcon,
    TwitterShareButton,
    WhatsappIcon,
    WhatsappShareButton
} from 'react-share'
import Blog from '../../models/blog'
import useCopy from '../../utils/copy_to_clipboard/use_copy'
import Toast from '../toast'
import styles from './blog_share_accordion.module.scss'

interface BlogShareAccordionProps {
    blog: Blog
}

const BlogShareAccordion: React.FC<BlogShareAccordionProps> = (
    { blog }
) => {
    const shareTitle = `${blog?.title} by NAAVIK Musik`
    const domain = process.env.NODE_ENV == "development"
        ? "localhost:3000"
        : process.env.NEXT_PUBLIC_DOMAIN
    const shareLink = `https://${domain}/blogs/${blog?.id}`
    const [expanded, setExpanded] = useState(false)
    const handleChange = (_, isExpanded: boolean) => {
        setExpanded(isExpanded)
    }
    const { copy, copied } = useCopy()
    const [textCopied, setTextCopied] = useState(false)
    const copyHandler = () => {
        const copyText = shareTitle + " " + shareLink
        copy(copyText)
    }
    useEffect(() => {
        setTextCopied(copied)
    }, [copied])
    const onClose = () => {
        setTextCopied(false)
    }
    return (
        <Accordion
            className={styles.accordion}
            onChange={handleChange}
            elevation={1}
        >
            <AccordionSummary
                className={styles.accordionSummary}
                expandIcon={
                    <IconButton>
                        <Share />
                    </IconButton>
                }
            >
                <div className={styles.mainTextAndButtons}>
                    <Typography
                        className={styles.shareText}
                        component="span"
                    >
                        Share this blog with your friends
                </Typography>
                    <ButtonGroup>
                        <Fade
                            in={!expanded}
                            exit={expanded}
                            timeout={400}
                        >
                            <FacebookShareButton
                                openShareDialogOnClick
                                quote={shareTitle}
                                url={shareLink}
                            >
                                <FacebookIcon
                                    size={30}
                                    round
                                />
                            </FacebookShareButton>
                        </Fade>
                        <Fade
                            in={!expanded}
                            exit={expanded}
                            timeout={500}
                        >
                            <TwitterShareButton
                                title={shareTitle}
                                url={shareLink}
                            >
                                <TwitterIcon
                                    size={30}
                                    round
                                />
                            </TwitterShareButton>
                        </Fade>
                    </ButtonGroup>
                </div>
            </AccordionSummary>
            <AccordionDetails>
                <ButtonGroup
                    className={styles.shareButtons}
                >
                    <Grow
                        in={expanded}
                        timeout={{ enter: 600 }}
                    >
                        <FacebookShareButton
                            openShareDialogOnClick
                            quote={shareTitle}
                            url={shareLink}
                        >
                            <FacebookIcon
                                size={30}
                                round
                            />
                        </FacebookShareButton>
                    </Grow>
                    <Grow
                        in={expanded}
                        timeout={{ enter: 700 }}
                    >
                        <TwitterShareButton
                            title={shareTitle}
                            url={shareLink}
                        >
                            <TwitterIcon
                                size={30}
                                round
                            />
                        </TwitterShareButton>
                    </Grow>
                    <Grow
                        in={expanded}
                        timeout={{ enter: 800 }}
                    >
                        <WhatsappShareButton
                            title={shareTitle}
                            url={shareLink}
                        >
                            <WhatsappIcon
                                size={30}
                                round
                            />
                        </WhatsappShareButton>
                    </Grow>
                    <Grow
                        in={expanded}
                        timeout={{ enter: 900 }}
                    >
                        <TelegramShareButton
                            title={shareTitle}

                            url={shareLink}
                        >
                            <TelegramIcon
                                size={30}
                                round
                            />
                        </TelegramShareButton>
                    </Grow>
                    <Grow
                        in={expanded}
                        timeout={{ enter: 1000 }}
                    >
                        <FacebookMessengerShareButton
                            title={shareTitle}
                            url={shareLink}
                            appId={process.env.NEXT_PUBLIC_FACEBOOK_APP_ID}
                        >
                            <FacebookMessengerIcon
                                size={30}
                                round
                            />
                        </FacebookMessengerShareButton>
                    </Grow>
                    <Grow
                        in={expanded}
                        timeout={{ enter: 1100 }}
                    >
                        <RedditShareButton
                            title={shareTitle}
                            url={shareLink}

                        >
                            <RedditIcon
                                size={30}
                                round
                            />
                        </RedditShareButton>
                    </Grow>
                    <Grow
                        in={expanded}
                        timeout={{ enter: 1200 }}
                        // @ts-ignore
                        className={styles.copyButton}
                    >
                        <IconButton
                            className={styles.copyButton}
                            onClick={copyHandler}
                        >
                            <FaClipboard
                                size={28}
                            />
                        </IconButton>
                    </Grow>
                </ButtonGroup>
            </AccordionDetails>
            <Toast
                open={textCopied}
                onClose={onClose}
                severity="success"
                message="Copied to clipboard"
            />
        </Accordion>
    )
}

export default BlogShareAccordion
