import {
    Dialog,
    DialogContent,
    DialogTitle,
    DialogContentText,
    DialogActions,
    Button
} from '@material-ui/core'
import React from 'react'
import styles from './confirmation_dialog.module.scss'

interface ConfirmationDialogProps {
    open: boolean
    onClose: any
    title: string
    closeButtonText: string
    confirmButtonText: string
    confirmAction: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void

}

const ConfirmationDialog: React.FC<ConfirmationDialogProps> = ({
    open,
    onClose,
    title,
    closeButtonText,
    confirmButtonText,
    confirmAction,
    children
}) => {
    return (
        <Dialog
            open={open}
            onClose={onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {children}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button
                    className={styles.button}
                    onClick={onClose}
                >
                    {closeButtonText}
                </Button>
                <Button
                    className={styles.button}
                    onClick={confirmAction}
                    autoFocus
                >
                    {confirmButtonText}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default ConfirmationDialog
