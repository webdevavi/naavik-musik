import {
    Button,
    Card,
    CardMedia,
    CircularProgress,
    Container
} from '@material-ui/core';
import { useEffect } from 'react'
import styles from './upload_image_form.module.scss'
import useUpload from '../../utils/upload/use_upload';
import { FaUpload } from 'react-icons/fa';
import Toast from '../toast';
import { useRouter } from 'next/router';

const UploadImageForm = () => {

    const {
        upload,
        uploading,
        uploaded,
        error,
        currentFileURL,
        clearNotifs
    } = useUpload()

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        clearNotifs()
    }

    const router = useRouter()

    useEffect(() => {
        if (uploaded) router.reload()
    }, [uploaded])

    const handleFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
        const files = Array<File>()
        if (e.target.files.length > 0) {
            for (let i = 0; i < e.target.files.length; i++) {
                files.push(e.target?.files?.item(i))
            }
        }
        upload(files)
    }

    return (
        <Container maxWidth="md">
            <Card className={styles.root} >
                <label htmlFor="upload-image">
                    <CardMedia
                        className={styles.image}
                        image={currentFileURL || "/plain-background.png"}
                    />
                    <input
                        id="upload-image"
                        name="upload-image"
                        type="file"
                        accept="image/*"
                        multiple
                        style={{ display: "none" }}
                        onChange={handleFileUpload}
                    />
                    <Button
                        component="span"
                        className={!uploading ? styles.uploadButton : styles.buttonDisabled}
                        variant="contained"
                        disabled={uploading}
                        endIcon={
                            !uploading
                                ? <FaUpload size={16} />
                                : <CircularProgress
                                    size={18}
                                    className={styles.buttonProgress}
                                />
                        }
                    >
                        {
                            !uploading
                                ? "Upload Image"
                                : "Uploading..."
                        }
                    </Button>
                </label>
            </Card>

            <Toast
                open={uploaded}
                onClose={handleClose}
                severity="success"
                message="Uploaded successfully!"
            />
            <Toast
                open={error != null}
                onClose={handleClose}
                severity="error"
                message={error}
            />
        </Container >
    )
}

export default UploadImageForm
