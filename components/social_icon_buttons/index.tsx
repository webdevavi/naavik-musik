import { ButtonGroup, Grow, IconButton } from '@material-ui/core'
import styles from './social_icon_buttons.module.scss'
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa'
import useVisibility from '../../utils/visibility/use_visibility'


interface SocialIconButtonsProps {
    inverted?: boolean
}

const socials = [
    {
        name: "facebook",
        className: styles.facebookIconButton,
        url: 'https://facebook.com/naavikmusik',
        Icon: FaFacebook,
    },
    {
        name: "instagram",
        className: styles.instagramIconButton,
        url: 'https://instagram.com/naavikmusik',
        Icon: FaInstagram
    },
    {
        name: "twitter",
        className: styles.twitterIconButton,
        url: 'https://twitter.com/naavikmusik',
        Icon: FaTwitter
    }
]

const SocialIconButtons: React.FC<SocialIconButtonsProps> = ({ inverted = false }) => {
    const { targetRef, enter } = useVisibility()

    return (
        <ButtonGroup
            className={styles.socialIconButtons}

        >
            {
                socials?.map(
                    (
                        {
                            name, className, url, Icon
                        },
                        index
                    ) => (
                            <Grow
                                key={index}
                                ref={targetRef}
                                in={enter}
                                timeout={{ enter: index * 200 + 1000 }}
                            >
                                <IconButton
                                    aria-label={name}
                                    className={className}
                                    onClick={() => window.open(url)}
                                >
                                    <Icon color={inverted ? "#5271FF" : "#E3EBEF"} size={30} />
                                </IconButton>
                            </Grow>
                        )
                )
            }
        </ButtonGroup>
    )
}

export default SocialIconButtons
