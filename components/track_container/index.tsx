import styles from './track_container.module.scss'
import Track from '../../models/track'
import { Button, Paper, Slide } from '@material-ui/core'
import Responsive from 'simple-responsive-react'
import { FaSpotify } from 'react-icons/fa'
import useVisibility from '../../utils/visibility/use_visibility'

interface TrackContainerProps { track: Track }

const TrackContainer: React.FC<TrackContainerProps> = ({ track }) => {
    const { targetRef, enter } = useVisibility()

    return (
        // @ts-ignore
        <div ref={targetRef}>
            <Slide
                direction="left"
                in={enter}
                timeout={400}
            >
                <Paper style={{ background: "none" }}>
                    <div className={styles.trackContainer}>


                        <div className={styles.albumImageAndPlayButtonContainer}>
                            <div className={styles.trackImageContainer}>
                                <Responsive type="only" screen="mobile">
                                    <img
                                        className={styles.trackImage}
                                        src={track?.album?.imageURL}
                                        alt={track?.name}
                                    />
                                </Responsive>
                                <Responsive type="above" screen="mobile">
                                    <img
                                        className={styles.trackImage}
                                        src={track?.album?.imageURL}
                                        alt={track?.name}
                                    />
                                </Responsive>
                                {
                                    track?.explicit && (
                                        <div className={styles.explicitContainer}>
                                            Explicit
                                        </div>
                                    )
                                }

                            </div>
                            <Responsive type="only" screen="mobile">
                                <Button
                                    className={styles.playButton}
                                    size="small"
                                    startIcon={<FaSpotify color="#1DB954" size={14} />}
                                    onClick={() => window.open(track?.link)}
                                >
                                    PLAY
                    </Button>
                            </Responsive>
                        </div>
                        <div className={styles.trackInfo}>
                            <div className={styles.trackName}>
                                {track?.name}
                            </div>
                            <div className={styles.trackAlbumName}>
                                {track?.album?.name}
                            </div>
                            <div className={styles.artistsName}>
                                {track?.artists?.map(
                                    artist => artist?.name
                                ).join(", ")}
                            </div>
                            <Responsive type="above" screen="mobile">
                                <Button
                                    className={styles.playButtonAboveMobile}
                                    size="small"
                                    onClick={() => window.open(track?.link)}
                                    startIcon={<FaSpotify color="#1DB954" />}
                                >

                                    PLAY
                    </Button>
                            </Responsive>
                        </div>
                    </div>
                </Paper>
            </Slide>
        </div>
    )
}

export default TrackContainer
