import { IconButton } from '@material-ui/core'
import React from 'react'
import { IconType } from 'react-icons/lib'
import styles from './menu_button.module.scss'

interface MenuButtonProps {
    color?: string
    Icon: IconType
    onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

const MenuButton: React.FC<MenuButtonProps> = (
    { color = "#168BC4", onClick, Icon }
) => {

    return (
        <IconButton
            className={styles.menuButton}
            onClick={onClick}
        >
            <Icon
                className={styles.menuIcon}
                size={36}
                color={color}
            />
        </IconButton>
    )
}

export default MenuButton