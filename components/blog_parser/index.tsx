import Markdown from 'markdown-to-jsx';
import React from 'react'
import Blog from '../../models/blog';
import overrides from './overrides'
import styles from './blog_parser.module.scss'

interface BlogParserProps {
    blog: Blog
}

const BlogParser: React.FC<BlogParserProps> = ({ blog }) => {
    return (
        <Markdown
            className={styles.parser}
            options={{ overrides }}
        >
            {blog?.content}
        </Markdown>
    )
}

export default BlogParser