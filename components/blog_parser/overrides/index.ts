import Link from "../../md_components/link"
import Parapgraph from "../../md_components/paragraph"
import Image from '../../md_components/image'
import headingOverrides from "./heading"
import tableOverrides from "./table"

const overrides = {
    ...headingOverrides,
    ...tableOverrides,
    p: {
        component: Parapgraph
    },
    img: {
        component: Image
    },
    a: {
        component: Link
    }
}

export default overrides
