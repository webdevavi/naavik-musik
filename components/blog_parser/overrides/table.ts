import { Table, TBody, TCell, TH, THead, TRow } from "../../md_components/table"

const tableOverrides = {
    table: {
        component: Table
    },
    tbody: {
        component: TBody
    },
    tr: {
        component: TRow
    },
    thead: {
        component: THead
    },
    th: {
        component: TH
    },
    td: {
        component: TCell
    }
}

export default tableOverrides