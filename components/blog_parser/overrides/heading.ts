import Heading from '../../md_components/heading'

const headingOverrides = {
    h1: {
        component: Heading,
        props: {
            as: "h1"
        }
    },
    h2: {
        component: Heading,
        props: {
            as: "h2"
        }
    },
    h3: {
        component: Heading,
        props: {
            as: "h3"
        }
    },
    h4: {
        component: Heading,
        props: {
            as: "h4"
        }
    },
    h5: {
        component: Heading,
        props: {
            as: "h5"
        }
    },
    h6: {
        component: Heading,
        props: {
            as: "h6"
        }
    }
}

export default headingOverrides