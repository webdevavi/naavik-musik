import useUploaded from '../../utils/upload/use_uploaded'
import {
    CircularProgress,
    Container,
    Grid,
    Typography
} from '@material-ui/core'
import styles from './uploaded_images.module.scss'
import ImageContainer from '../image_container'
import SectionHeader from '../section_header'
import Toast from '../toast'

const UploadedImages = () => {
    const {
        loading,
        images,
        error,
        clearNotifs
    } = useUploaded()
    return (
        <Container maxWidth="md">
            <SectionHeader title="Uploaded Images" />
            <div className={styles.imagesContainer}>
                {
                    !loading && images?.map(
                        image => (
                            <ImageContainer key={image.path} image={image} />
                        )
                    )
                }

                {
                    loading
                        ? <CircularProgress
                            size={24}
                            className={styles.progress}
                        />
                        : !images || images?.length == 0 && (
                            <Typography
                                className={styles.noImagesText}
                                component="span"
                            >
                                No images uploaded yet!
                            </Typography>
                        )
                }
            </div>
            <Toast
                open={error != null}
                onClose={clearNotifs}
                severity="error"
                message={error}
            />
        </Container>
    )
}

export default UploadedImages
