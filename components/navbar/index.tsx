import { Button, ButtonGroup, CircularProgress, Container } from "@material-ui/core"
import styles from './navbar.module.scss'
import { useRouter } from 'next/router'
import React, { useState } from "react"
import MenuButton from "../menu_button"
import { MdMenu } from 'react-icons/md'
import useAuth from "../../utils/auth/use_auth"
import Toast from "../toast"
import ConfirmationDialog from "../confirmation_dialog"

interface NavbarProps {
    toggleDrawer: (open: boolean) => void
}

const Navbar: React.FC<NavbarProps> = ({ toggleDrawer }) => {
    const [open, setOpen] = useState(false)

    const router = useRouter()
    const {
        token,
        logout,
        loggingOut,
        loggedOut,
        error,
        closeNotifs
    } = useAuth()

    React.useEffect(() => {
        if (loggedOut) {
            if (router.pathname == "/") router.reload()
            else router.replace("/")
        }
    }, [loggedOut])

    const handleLogout = async () => {
        await logout()
        setOpen(false)
    }

    return <nav>
        <div className={styles.navbarContainer}>
            <Container className={styles.navbar} maxWidth="md">
                <div className={styles.logoContainer}>
                    <img
                        className={styles.logo}
                        src="/logo-black.png"
                        alt="NAAVIK Musik black logo"
                    />
                </div>
                <ButtonGroup className={styles.buttonGroup}>
                    {token &&
                        <Button
                            className={styles.logoutButton}
                            variant="outlined"
                            size="small"
                            onClick={() => setOpen(true)}
                        >
                            {!loggedOut ? loggingOut ? "Logging out" : "Logout" : "Logged out"}
                            {
                                loggingOut && <CircularProgress
                                    size={24}
                                    className={styles.buttonProgress}
                                />
                            }
                        </Button>
                    }

                    <div className={styles.menuButton}>
                        <MenuButton Icon={MdMenu} onClick={() => toggleDrawer(true)} />
                    </div>
                </ButtonGroup>
            </Container>
            <Toast
                open={error != null}
                onClose={closeNotifs}
                severity="error"
                message={error}
            />

            <ConfirmationDialog
                title="Logout"
                open={open}
                onClose={() => setOpen(false)}
                closeButtonText="Cancel"
                confirmButtonText="Sure"
                confirmAction={handleLogout}
            >
                This will log you out and end your session, you're sure?
            </ConfirmationDialog>

        </div>
    </nav >
}

export default Navbar