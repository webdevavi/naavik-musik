import React from 'react'
import { Container, Typography } from '@material-ui/core'
import Responsive from 'simple-responsive-react'
import Artist from '../../models/artist'
import SocialIconButtons from '../social_icon_buttons'
import styles from './hero.module.scss'

interface HeroProps {
    artist: Artist
}

const Hero: React.FC<HeroProps> = (
    { artist }
) => {
    return (
        <div className={styles.hero}>
            <Container
                maxWidth="md"
                className={styles.heroContainer}
            >
                <div className={styles.headerContainer}>
                    <h1 className={styles.header}>
                        NAAVIK MUSIK
                    </h1>
                    <Responsive
                        type="above"
                        screen="mobile"
                    >
                        <Typography className={styles.socialHeader} component="h2">
                            Find us at
                        </Typography>
                        <SocialIconButtons />
                    </Responsive>
                </div>
                <div className={styles.imageContainer}>
                    <img
                        className={styles.image}
                        src="/logo-shape-blue.png"
                        alt="Composing Music"
                    />
                </div>
                <Responsive
                    type="only"
                    screen="mobile"
                >
                    <Typography className={styles.socialHeader} component="h2">
                        Find us at
                    </Typography>
                    <SocialIconButtons />
                </Responsive>
            </Container>
        </div >
    )
}

export default Hero