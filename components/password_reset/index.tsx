import {
    Button,
    Container,
    TextField,
    CircularProgress,
    Backdrop,
    Typography
} from '@material-ui/core'

import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import styles from './password_reset.module.scss'
import useAuth from '../../utils/auth/use_auth';
import { CheckCircle } from '@material-ui/icons';
import Toast from '../toast';

interface Values {
    email: string
}

const initialValues: Values = {
    email: ''
}

const validationSchema = Yup.object({
    email: Yup.string().email().label("Email"),
})

const PasswordReset = () => {
    const {
        resetPassword,
        emailForPasswordResetSending,
        emailForPasswordResetSent,
        error,
        closeNotifs
    } = useAuth()

    const handleSubmit = (values: Values) => resetPassword(values.email)

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        closeNotifs()
    }

    if (!emailForPasswordResetSent) {

        return (
            <Container maxWidth="md">
                <Backdrop
                    className={styles.backdrop}
                    open={emailForPasswordResetSent || emailForPasswordResetSending}
                />
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={(values) => handleSubmit(values)}
                >
                    {
                        props => (
                            <Form className={styles.form}>
                                <Field name="email">
                                    {
                                        ({ field, form }) => (
                                            <TextField
                                                name="email"
                                                id="email"
                                                label="Email"
                                                {...field}
                                                helperText={form.touched.email ? form.errors.email : ""}
                                                error={form.touched.email && Boolean(form.errors.email)}
                                            />
                                        )
                                    }

                                </Field>

                                <Button
                                    className={emailForPasswordResetSending ? styles.buttonDisabled : styles.button}
                                    variant="contained"
                                    type="submit"
                                    disabled={emailForPasswordResetSending}
                                >
                                    {emailForPasswordResetSending ? "Sending" : "Send reset link"}
                                    {
                                        emailForPasswordResetSending && <CircularProgress
                                            size={24}
                                            className={styles.buttonProgress}
                                        />
                                    }

                                </Button>
                            </Form>
                        )
                    }

                </Formik>

                <Toast
                    open={error != null}
                    onClose={handleClose}
                    severity="error"
                    message={error}
                />
            </Container >
        )
    } else return (
        <Container maxWidth="md" className={styles.emailSentContainer}>

            <CheckCircle
                className={styles.checkIcon}
            />
            <Typography
                className={styles.emailSentText}
                component="p"
            >
                Check your email, a link to reset your password has been sent!
            </Typography>

        </Container>
    )
}

export default PasswordReset