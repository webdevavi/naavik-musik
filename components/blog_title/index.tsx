import { Container, Typography } from "@material-ui/core"
import styles from './blog_title.module.scss'

interface BlogTitleProps {
    title: string
}

const BlogTitle: React.FC<BlogTitleProps> = ({ title }) => {
    return (
        <div
            className={styles.titleContainer}
        >
            <Container
                maxWidth="md"
            >
                <Typography
                    className={styles.title}
                    component="h1"
                >
                    {title}
                </Typography>
            </Container>
        </div>
    )
}

export default BlogTitle