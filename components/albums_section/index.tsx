import React, { useState } from 'react'
import styles from './albums_section.module.scss'
import Album from '../../models/album'
import { Container, Fade, Grow, Paper, Slide } from '@material-ui/core'
import SectionHeader from '../section_header'
import AlbumInfoContainer from '../album_info_container'
import Divider from '../divider'
import Carousel from 'react-material-ui-carousel'

interface AlbumsSectionProps {
    albums: Album[]
}

const AlbumsSection: React.FC<AlbumsSectionProps> = (
    { albums }
) => {
    const [index, setIndex] = useState(0)
    const [fadeIn, setFadeIn] = useState(true)

    const changeIndex = (index: number) => {
        setFadeIn(false)
        setTimeout(() => {
            setFadeIn(true)
            setIndex(index)
        }, 200);
    }
    return (
        <>
            <Divider />
            <div className={styles.albumsSection}>
                <Container maxWidth="md">
                    <SectionHeader title="Latest Albums" />
                    <div className={styles.albumsContainer}>
                        <Carousel
                            onChange={changeIndex}
                            navButtonsAlwaysVisible
                            interval={6000}
                            timeout={400}
                            animation="slide"
                        >
                            {albums?.map(
                                album => (
                                    <div key={album?.id} className={styles.imgContainer}>
                                        <img
                                            className={styles.albumImage}
                                            src={album?.imageURL}
                                            alt={album?.name}
                                        />
                                    </div>
                                )
                            )}
                        </Carousel>
                        <Fade
                            in={fadeIn}
                            timeout={400}
                        >
                            <Paper
                                style={{ background: "none" }}
                            >
                                <AlbumInfoContainer album={albums[index]} />
                            </Paper>
                        </Fade>

                        {/* {
                            albums?.map(
                                (album, i) => (
                                    <Slide
                                        direction="right"
                                        in={index == i}
                                        exit={index != i}
                                        mountOnEnter
                                    >
                                        <Paper>
                                            <AlbumInfoContainer album={album} />
                                        </Paper>
                                    </Slide>
                                )
                            )
                        } */}

                    </div>
                </Container>
            </div>
        </>
    )
}

export default AlbumsSection