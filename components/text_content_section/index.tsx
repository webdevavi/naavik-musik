import React from 'react'
import { Container, Fade, Grow, Slide, Typography } from '@material-ui/core'
import Responsive from 'simple-responsive-react'
import styles from './text_content_section.module.scss'
import useVisibility from '../../utils/visibility/use_visibility'

interface TextContentSectionProps {
    imagePosition?: "left" | "right"
    imageSrc: string
    headerText: string
}

const TextContentSection: React.FC<TextContentSectionProps> = (
    { imageSrc, imagePosition = "left", headerText, children }
) => {
    const { targetRef, enter } = useVisibility()

    return (
        // @ts-ignore
        <div ref={targetRef} className={styles.aboutSection}>
            <Container maxWidth="md">
                <div className={styles.headerContainer}>
                    <Grow in={enter} timeout={1000}>

                        <Typography
                            variant="h2"
                            component="h2"
                            className={styles.header}
                        >
                            {headerText}
                        </Typography>
                    </Grow>
                </div>
                <Responsive type="above" screen="mobile">
                    <div className={styles.aboutContainer}>
                        {
                            imagePosition === "left"
                                ? (
                                    <>
                                        <Fade in={enter} timeout={1000}>
                                            <div className={styles.imgContainer}>
                                                <img
                                                    className={styles.image}
                                                    src={imageSrc}
                                                    alt={headerText}
                                                />
                                            </div>
                                        </Fade>
                                        <Slide
                                            direction="left"
                                            in={enter}
                                            timeout={1000}
                                        >
                                            <div className={styles.textContainerRight}>
                                                {children}
                                            </div>
                                        </Slide>
                                    </>
                                )
                                : (
                                    <>
                                        <Slide
                                            direction="right"
                                            in={enter}
                                            timeout={1000}
                                        >
                                            <div className={styles.textContainerLeft}>
                                                {children}
                                            </div>
                                        </Slide>
                                        <Fade in={enter} timeout={1000}>

                                            <div className={styles.imgContainer}>
                                                <img
                                                    className={styles.image}
                                                    src={imageSrc}
                                                    alt={headerText}
                                                />
                                            </div>
                                        </Fade>
                                    </>
                                )
                        }
                    </div>
                </Responsive>
                <Responsive type="only" screen="mobile">
                    <div className={styles.aboutContainer}>
                        <Fade in={enter} timeout={1000}>
                            <div className={styles.imgContainer}>
                                <img
                                    className={styles.image}
                                    src={imageSrc}
                                    alt={headerText}
                                />
                            </div>
                        </Fade>
                        <Slide
                            direction="left"
                            in={enter}
                            timeout={1000}
                        >
                            <div className={styles.textContainer}>
                                {children}
                            </div>
                        </Slide>
                    </div>
                </Responsive>
            </Container>
        </div>
    )
}

export default TextContentSection
