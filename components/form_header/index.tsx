import { Typography } from '@material-ui/core'
import styles from './form_header.module.scss'

interface FormHeaderProps {
    title?: string
}

const FormHeader: React.FC<FormHeaderProps> = ({ title = "NAAVIK Musik" }) => {
    return (
        <div className={styles.header}>
            <div className={styles.logoContainer}>
                <img
                    className={styles.logo}
                    src="/logo-white.png"
                />
            </div>
            <Typography
                className={styles.headerText}
                variant="h1"
                component="h1"
            >
                {title}
            </Typography>
        </div>
    )
}

export default FormHeader