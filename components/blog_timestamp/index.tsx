import React from 'react'
import { formatDistance } from 'date-fns'
import { Chip } from '@material-ui/core'
import { CalendarToday } from '@material-ui/icons'
import styles from './blog_timestamp.module.scss'

interface BlogTimestamp {
    createdAt?: Date
    modifiedAt?: Date
}

const BlogTimestamp: React.FC<BlogTimestamp> = (
    { createdAt, modifiedAt }
) => {
    const createdAtDistance = formatDistance(
        createdAt || new Date(), new Date(), { addSuffix: true }
    )
    const modifiedAtDistance = formatDistance(
        modifiedAt || new Date(), new Date(), { addSuffix: true }
    )
    return (
        <div className={styles.chips}>
            {
                !modifiedAt ? <Chip
                    className={styles.chip}
                    variant="outlined"
                    label={"posted " + createdAtDistance}
                    size="small"
                    icon={<CalendarToday />}
                />
                    : <Chip
                        className={styles.chip}
                        variant="outlined"
                        label={"edited " + modifiedAtDistance}
                        size="small"
                        icon={<CalendarToday />}
                    />
            }

        </div>
    )
}

export default BlogTimestamp
