import {
    Backdrop,
    Button,
    CircularProgress,
    Container,
    TextField
} from '@material-ui/core'
import { Field, Form, Formik, FormikProps } from 'formik'
import React, { useEffect } from 'react'
import useBlog from '../../utils/blog/use_blog'
import Toast from '../toast'
import styles from './create_blog_form.module.scss'
import * as Yup from 'yup'
import { Blog } from '../../models/blog'
import { Image } from '../../models/image'
import { useRouter } from 'next/router'

interface Values {
    title: string
    description: string
    content: string
    thumbnail: string
}

const validationSchema = Yup.object({

    title: Yup.string()
        .min(30)
        .max(100)
        .required()
        .label("Title"),

    description: Yup.string()
        .min(50)
        .max(150)
        .required()
        .label("Description"),

    content: Yup.string()
        .min(100)
        .required()
        .label("Content"),

    thumbnail: Yup
        .string()
        .url()
        .required()
        .label("Thumbnail's URL")

})

interface CreateBlogFormProps {
    editing?: boolean
    blog?: Blog
}

const CreateBlogForm: React.FC<CreateBlogFormProps> = (
    { editing = false, blog }
) => {

    const initialValues: Values = {
        title: editing ? blog?.title : "",
        description: editing ? blog?.description : "",
        content: editing ? blog?.content : "",
        thumbnail: editing ? blog?.thumbnail?.url : "",
    }

    const {
        create,
        edit,
        creating,
        created,
        blogId,
        error,
        clearNotifs
    } = useBlog()

    const handleSubmit = ({
        title,
        description,
        content,
        thumbnail,
    }: Values) => {
        const newBlog = new Blog({
            title,
            description,
            content,
            thumbnail: new Image({ url: thumbnail }),
        })
        editing ? edit(newBlog, blog?.id) : create(newBlog)
    }

    const router = useRouter()

    useEffect(() => {
        const id = editing ? blog?.id : blogId
        if (created) router.push("/blogs/" + id)
    }, [created])

    return (
        <Container maxWidth="md">
            <Backdrop
                className={styles.backdrop}
                open={created || creating}
            />
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => handleSubmit(values)}
            >
                {
                    (props: FormikProps<Values>) => (
                        <Form className={styles.form}>
                            <aside className={styles.leftAside}>
                                <Field name="title">
                                    {
                                        ({ field, form }) => (
                                            <TextField
                                                fullWidth
                                                name="title"
                                                id="title"
                                                label="Title"
                                                {...field}
                                                helperText={form.touched.title ? form.errors.title : ""}
                                                error={form.touched.title && Boolean(form.errors.title)}
                                            />
                                        )
                                    }
                                </Field>

                                <Field name="description">
                                    {
                                        ({ field, form }) => (
                                            <TextField
                                                fullWidth
                                                name="description"
                                                id="description"
                                                label="Description"
                                                {...field}
                                                helperText={form.touched.description ? form.errors.description : ""}
                                                error={form.touched.description && Boolean(form.errors.description)}
                                            />
                                        )
                                    }
                                </Field>

                                <Field name="content">
                                    {
                                        ({ field, form }) => (
                                            <TextField
                                                multiline
                                                fullWidth
                                                name="content"
                                                id="content"
                                                label="Content"
                                                {...field}
                                                helperText={form.touched.content ? form.errors.content : ""}
                                                error={form.touched.content && Boolean(form.errors.content)}
                                            />
                                        )
                                    }
                                </Field>
                            </aside>

                            <aside className={styles.rightAside}>

                                <Field name="thumbnail">
                                    {
                                        ({ field, form }) => (
                                            <TextField
                                                fullWidth
                                                name="thumbnail"
                                                id="thumbnail"
                                                label="Thumbnail"
                                                {...field}
                                                helperText={form.touched.thumbnail ? form.errors.thumbnail : ""}
                                                error={form.touched.thumbnail && Boolean(form.errors.thumbnail)}
                                            />
                                        )
                                    }
                                </Field>

                                <div className={styles.thumbnailContainer}>
                                    <img
                                        className={styles.thumbnail}
                                        src={props.values.thumbnail || '/plain-background.png'}
                                        alt={props.values.title}
                                    />
                                </div>

                                <Button
                                    className={created ? styles.buttonDisabled : styles.button}
                                    variant="contained"
                                    type="submit"
                                    disabled={created || creating}
                                >
                                    {!created ? creating ? "Saving" : "Save" : "Saved"}
                                    {
                                        creating && <CircularProgress
                                            size={24}
                                            className={styles.buttonProgress}
                                        />
                                    }
                                </Button>

                            </aside>



                        </Form>
                    )
                }

            </Formik>

            <Toast
                open={created}
                onClose={clearNotifs}
                severity="success"
                message="Blog created successfully!"
            />

            <Toast
                open={error != null}
                onClose={clearNotifs}
                severity="error"
                message={error}
            />
        </Container >
    )
}

export default CreateBlogForm
