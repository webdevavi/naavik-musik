import { Button, CircularProgress, Grow, TextField, Typography } from '@material-ui/core'
import { FaCheckCircle, FaEnvelopeOpenText } from 'react-icons/fa'
import { Field, Form, Formik } from 'formik'
import React from 'react'
import * as Yup from 'yup'
import Message from '../../models/message'
import useContact from '../../utils/contact/use_contact'
import styles from './contact_form.module.scss'

interface Values {
    email: string
    name: string
    message: string
}

const initialValues: Values = {
    email: "",
    name: "",
    message: ""
}

const validationSchema = Yup.object({
    email: Yup.string()
        .email()
        .required()
        .label("Email"),
    name: Yup.string()
        .min(3)
        .required()
        .label("Name"),
    message: Yup.string()
        .min(144)
        .required()
        .label("Message")
})

const ContactForm = () => {
    const {
        send,
        sending,
        sent,
        error,
        clearNotifs
    } = useContact()
    const handleSubmit = (values: Values) => {
        const newMessage = Message.fromJSON(values)
        send(newMessage)
    }

    return (
        <div className={styles.root} id="contact-us">
            <Typography
                className={styles.header}
                component="h2"
            >
                Contact us now
            </Typography>
            {  error == null
                ? !sent
                    ? <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                        onSubmit={handleSubmit}
                    >
                        <Form className={styles.form}>
                            <Field name="name">
                                {({ field, form }) => (
                                    <TextField
                                        name="name"
                                        id="name"
                                        label="Name"
                                        type="text"
                                        {...field}
                                        helperText={form.touched.name ? form.errors.name : ""}
                                        error={form.touched.name && Boolean(form.errors.name)}
                                    />
                                )}
                            </Field>
                            <Field name="email">
                                {({ field, form }) => (
                                    <TextField
                                        name="email"
                                        id="email"
                                        label="Email"
                                        type="text"
                                        {...field}
                                        helperText={form.touched.email ? form.errors.email : ""}
                                        error={form.touched.email && Boolean(form.errors.email)}
                                    />
                                )}
                            </Field>
                            <Field name="message">
                                {({ field, form }) => (
                                    <TextField
                                        variant="filled"
                                        className={styles.messageInput}
                                        multiline
                                        name="message"
                                        id="message"
                                        label="Message"
                                        type="text"
                                        {...field}
                                        helperText={form.touched.message ? form.errors.message : ""}
                                        error={form.touched.message && Boolean(form.errors.message)}
                                    />
                                )}
                            </Field>
                            <Button
                                className={sent || sending ? styles.buttonDisabled : styles.button}
                                variant="contained"
                                type="submit"
                                disabled={sent || sending}
                            >
                                {!sent ? sending ? "Sending" : "Send" : "Sent"}
                                {
                                    sending && <CircularProgress
                                        size={24}
                                        className={styles.buttonProgress}
                                    />
                                }
                            </Button>
                        </Form>

                    </Formik>
                    : (
                        <div className={styles.sentMessageContainer}>

                            <FaCheckCircle size={36} />

                            <Typography className={styles.sentText}>
                                We have received your message!
                            </Typography>
                            <Grow in={sent}>
                                <FaEnvelopeOpenText size={80} />
                            </Grow>
                        </div>
                    )
                : (
                    <div className={styles.errorMessageContainer}>
                        <Typography className={styles.errorText}>
                            Uh-oh! Some error occured!
                    </Typography>
                        <Button
                            className={styles.sendAgainButton}
                            variant="outlined"
                            onClick={clearNotifs}
                        >
                            Send Again
                        </Button>
                    </div>
                )
            }

        </div>
    )
}

export default ContactForm