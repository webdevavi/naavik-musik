import { NextPageContext } from 'next';
import Router from 'next/router';

export default (ctx: NextPageContext, target: string) => {
    if (ctx.res) {
        // server 
        ctx.res.writeHead(303, { Location: target });
        ctx.res.end();
    } else {
        // client
        Router.replace(target);
    }
}
