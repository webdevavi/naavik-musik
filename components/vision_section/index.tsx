import React from 'react'
import TextContentSection from '../text_content_section'

const VisionSection = () => {
    return (
        <TextContentSection
            imageSrc="/vision_section_image.svg"
            imagePosition="right"
            headerText="VISION"
        >
            <p>There’s a plethora of music all around us, but how much of it do we consume and inspire from?</p>
            <p>The real music that we seek, evolves from time to time. And NAAVIK Musik is all about evolution.</p>
            <p>Our vision holds the key to never stop at a certain space if there is room to evolve more.</p>
        </TextContentSection>
    )
}

export default VisionSection
