import {
    Button,
    Card,
    CardActions,
    CardHeader,
    CardMedia,
    CircularProgress,
    IconButton
} from '@material-ui/core'
import { Check, Delete, Link } from '@material-ui/icons'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { Image } from '../../models/image'
import useCopy from '../../utils/copy_to_clipboard/use_copy'
import useUploaded from '../../utils/upload/use_uploaded'
import ConfirmationDialog from '../confirmation_dialog'
import Toast from '../toast'
import styles from './image_container.module.scss'

interface ImageContainerProps {
    image: Image
}

const ImageContainer: React.FC<ImageContainerProps> = ({ image }) => {
    const { url, path } = image
    const {
        remove,
        deleting,
        deleted,
        error,
        clearNotifs
    } = useUploaded()

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleRemove = () => {
        remove(image)
        setOpen(false)
    }

    const router = useRouter()

    useEffect(() => {
        if (deleted) router.reload()
    }, [deleted])

    const { copy, copied } = useCopy()

    return (
        <>
            <Card className={styles.root}>
                <CardHeader
                    className={styles.header}
                    title={image?.name?.slice(0, 40)}
                />
                <CardMedia
                    className={styles.media}
                    image={url}
                    title={url}
                    onClick={() => window.open(url)}
                />
                <CardActions disableSpacing>
                    <Button
                        className={styles.button}
                        variant="outlined"
                        endIcon={
                            copied
                                ? <Check className={styles.icon} />
                                : <Link className={styles.icon} />
                        }
                        size="small"
                        onClick={() => copy(url)}
                    >
                        {copied ? 'Copied' : 'Copy Link'}
                    </Button>
                    <IconButton
                        className={styles.deleteButton}
                        onClick={handleClickOpen}
                    >
                        {
                            deleting
                                ? <CircularProgress
                                    size={20}
                                    className={styles.buttonProgress}
                                />
                                : <Delete className={styles.icon} fontSize="small" />
                        }
                    </IconButton>
                </CardActions>
            </Card>


            <ConfirmationDialog
                open={open}
                title="Delete image"
                onClose={handleClose}
                closeButtonText="No"
                confirmButtonText="Yes"
                confirmAction={() => handleRemove()}
            >
                This will delete the image permanently, are you sure you want to delete?
            </ConfirmationDialog>
            <Toast
                open={error != null}
                message={error}
                onClose={clearNotifs}
                severity="error"
            />
            <Toast
                open={deleted}
                message="Deleted successfully!"
                onClose={clearNotifs}
                severity="success"
            />
        </>
    )
}

export default ImageContainer
