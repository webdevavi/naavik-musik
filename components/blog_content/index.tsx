import React, { useEffect, useState } from 'react'
import { Blog } from '../../models/blog'
import styles from './blog_content.module.scss'
import BlogShareAccordion from '../blog_share_accordion'
import BlogParser from '../blog_parser'
import BlogTimestamp from '../blog_timestamp'
import { ButtonGroup, CircularProgress, IconButton } from '@material-ui/core'
import { Delete } from '@material-ui/icons'
import { FaEdit } from 'react-icons/fa'
import useAuth from '../../utils/auth/use_auth'
import useBlog from '../../utils/blog/use_blog'
import ConfirmationDialog from '../confirmation_dialog'
import Toast from '../toast'
import { useRouter } from 'next/router'

interface BlogContentProps {
    blog: Blog
}

const BlogContent: React.FC<BlogContentProps> = ({ blog }) => {
    const { token } = useAuth()
    const {
        remove,
        removed,
        removing,
        error,
        clearNotifs
    } = useBlog()
    const [deleteOpen, setDeleteOpen] = useState(false)
    const router = useRouter()
    useEffect(() => {
        if (removed) router.replace("/blogs")
    }, [removed])
    const handleRemove = () => {
        setDeleteOpen(false)
        remove(blog?.id)
    }
    return (
        <div className={styles.container}>
            <div className={styles.blogHeader}>
                <div className={styles.thumbnailContainer}>
                    <img
                        className={styles.thumbnail}
                        src={blog?.thumbnail?.url}
                        alt={blog?.title}
                    />
                </div>
                <div className={styles.optionsBar}>
                    <BlogTimestamp
                        createdAt={blog?.createdAt}
                        modifiedAt={blog?.modifiedAt}
                    />
                    {token && <ButtonGroup>
                        <IconButton
                            size="small"
                            onClick={() => router.push("/blogs/edit/" + blog?.id)}
                        >
                            <FaEdit />
                        </IconButton>
                        <IconButton
                            size="small"
                            onClick={() => setDeleteOpen(true)}
                        >
                            {removing
                                ? <CircularProgress
                                    className={styles.progress}
                                />
                                : <Delete />}
                        </IconButton>
                    </ButtonGroup>}
                </div>
                <BlogShareAccordion blog={blog} />
            </div>
            {blog && <BlogParser blog={blog} />}
            <ConfirmationDialog
                open={deleteOpen}
                onClose={() => setDeleteOpen(false)}
                closeButtonText="Cancel"
                confirmButtonText="Delete"
                confirmAction={handleRemove}
                title="Delete"
            >
                This will delete this blog permanently, are you sure?
            </ConfirmationDialog>
            <Toast
                open={removed}
                onClose={clearNotifs}
                severity="success"
                message="Blog removed successfully!"
            />
            <Toast
                open={error != null}
                onClose={clearNotifs}
                severity="error"
                message={error}
            />
        </div >
    )
}

export default BlogContent
