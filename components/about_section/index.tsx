import React from 'react'
import TextContentSection from '../text_content_section'

const AboutSection = () => {
    return (
        <TextContentSection
            imageSrc="/about_section_image.svg"
            headerText="ABOUT NAAVIK MUSIK"
        >
            <p>NAAVIK is a person who rows the boat and shows you direction by cutting through the water. By guiding you alongside, and creating a path that has never been discovered by the travelers, he shows you an entire new Universe.</p>

            <p>He guides you so that you can enjoy the journey, seamlessly.</p>

            <p>Similarly, NAAVIKMUSIK helps artists to streamline their journey of music with the appropriate direction and support required.</p>

            <p><strong>FIND YOUR MUSIC</strong></p>
        </TextContentSection>
    )
}

export default AboutSection
