import { Avatar, Card, CardContent, CardHeader, Divider, LinearProgress, Link, List, ListItem, ListItemAvatar, ListItemText } from '@material-ui/core'
import React, { useEffect } from 'react'
import useBlog from '../../utils/blog/use_blog'
import styles from './recent_blogs.module.scss'

const RecentBlogs = () => {
    const {
        get,
        getting,
        blogs,
        error
    } = useBlog()

    useEffect(() => {
        get(10)
    }, [])

    return (
        <>
            <Card className={styles.card}>
                <CardHeader
                    className={styles.cardHeader}
                    title="Recent Blogs"
                    component="h3"
                />
                <CardContent className={styles.cardComponent}>
                    {getting
                        ? <LinearProgress />
                        : error == null
                            ? <List className={styles.list}>
                                {
                                    blogs?.map(
                                        (blog, index) => (
                                            <Link
                                                href={`/blogs/${blog?.id}`}
                                                key={blog?.id}
                                            >
                                                <ListItem
                                                    className={styles.listItem}
                                                    alignItems="flex-start"
                                                >
                                                    <ListItemAvatar>
                                                        <Avatar
                                                            alt={blog?.title}
                                                            src={blog?.thumbnail?.url}
                                                            variant="square"
                                                        />
                                                    </ListItemAvatar>
                                                    <ListItemText
                                                        className={styles.listItemText}
                                                        primary={blog?.title}
                                                        secondary={blog?.description}
                                                    />

                                                </ListItem>
                                                {index != blogs?.length - 1 && <Divider variant="inset" component="li" />}
                                            </Link>
                                        )
                                    )
                                }

                            </List>
                            : <div className={styles.error}>
                                {error}
                            </div>
                    }
                </CardContent>
            </Card >

        </>
    )
}

export default RecentBlogs
