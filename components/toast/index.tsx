import { Grow, Snackbar, SnackbarCloseReason } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'
import React from 'react'

interface ToastProps {
    open?: boolean
    onClose?: ((event: React.SyntheticEvent<any, Event>, reason: SnackbarCloseReason) => void) | ((event: React.SyntheticEvent<Element, Event>) => void)
    message?: string | null
    severity?: "success" | "error" | "info" | "warning"
}

const Toast: React.FC<ToastProps> = ({
    open = false,
    onClose,
    message,
    severity = "success"
}) => {
    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={open}
            autoHideDuration={6000}
            onClose={onClose}
            TransitionComponent={Grow}
        >
            <MuiAlert
                elevation={6}
                variant="filled"
                onClose={onClose as (event: React.SyntheticEvent<Element, Event>) => void}
                severity={severity}
            >
                {message}
            </MuiAlert>
        </Snackbar>
    )
}

export default Toast
