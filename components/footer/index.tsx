import React from 'react'
import { Container, Typography } from '@material-ui/core'
import SocialIconButtons from '../social_icon_buttons'
import styles from './footer.module.scss'
import ContactForm from '../contact_form'
const Footer = () => {
    return (
        <footer className={styles.footer}>
            <Container maxWidth="md" className={styles.footerContainer}>
                <div className={styles.footerInfo}>
                    <div className={styles.logoContainer}>
                        <img
                            className={styles.logo}
                            src="/logo-blue.png"
                        />
                    </div>
                    <ContactForm />
                    <div className={styles.socials}>
                        <SocialIconButtons />
                    </div>
                </div>

                <Typography variant='h4' className={styles.copyrightText}>
                    <span className={styles.copyrightSymbol}>
                        © {" "}
                    </span>
                    NAAVIK MUSIK {" "}
                    {new Date().getFullYear().toString()}
                </Typography>
            </Container>
        </footer>
    )
}

export default Footer
