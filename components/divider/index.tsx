import { Grow } from '@material-ui/core'
import useVisibility from '../../utils/visibility/use_visibility'
import styles from './divider.module.scss'

interface DividerProps {
    inverted?: boolean
}

const Divider: React.FC<DividerProps> = ({ inverted = false }) => {
    const { targetRef, enter } = useVisibility()
    return (
        // @ts-ignore
        <div ref={targetRef}>
            <Grow in={enter} timeout={800}>
                <div className={inverted ? styles.light : styles.dark} />
            </Grow>
        </div>
    )
}

export default Divider
