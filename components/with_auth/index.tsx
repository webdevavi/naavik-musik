import { NextPage, NextPageContext } from 'next'
import cookies from 'next-cookies'
import React from 'react'
import redirect from '../redirect'

const withAuth = (
    Page: NextPage,
    redirectTo: string,
    callback?: (ctx: NextPageContext) => {}
) => {
    return class AuthComponent extends React.Component {
        static async getInitialProps(ctx: NextPageContext) {
            const { auth } = cookies(ctx)
            if (!auth || auth == null || auth == undefined || auth == "undefined") {
                redirect(ctx, "/login?redirect=" + redirectTo)
                return {}
            }
            return callback ? callback(ctx) : {}
        }
        render() {
            return <Page {...this.props} />
        }
    }
}
export default withAuth