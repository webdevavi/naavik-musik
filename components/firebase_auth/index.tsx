import {
    Button,
    Container,
    TextField,
    CircularProgress,
    Backdrop,
    Typography, IconButton
} from '@material-ui/core'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import styles from './firebase_auth.module.scss'
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router';
import useAuth from '../../utils/auth/use_auth';
import { CheckCircle, Visibility, VisibilityOff } from '@material-ui/icons';
import Toast from '../toast';
import ConfirmationDialog from '../confirmation_dialog'

interface Values {
    email: string
    password: string
}

const initialValues: Values = {
    email: '',
    password: ''
}

const validationSchema = Yup.object({
    email: Yup.string().email().label("Email"),
    password: Yup.string().min(6).label("Password")
})

interface FirebaseAuthProps {
    redirect?: string
}

const FirebaseAuth: React.FC<FirebaseAuthProps> = ({ redirect }) => {

    const [open, setOpen] = useState(false)

    const toggle = (open: boolean) => setOpen(open)

    const [passwordVisible, setPasswordVisible] = useState(false)

    const togglePasswordVisibility = () => {
        setPasswordVisible(state => !state)
    }

    const router = useRouter()
    const {
        token,
        login,
        loggingIn,
        loggedIn,
        logout,
        loggingOut,
        loggedOut,
        error,
        closeNotifs
    } = useAuth()

    const handleSubmit = (values: Values) => login(values.email, values.password)

    const handleLogout = () => logout()

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        closeNotifs()
    }

    useEffect(() => {
        if (token && redirect) router.replace(redirect)
    }, [token])

    useEffect(() => {
        if (loggedIn) {
            if (redirect != null || redirect != undefined) router.replace(redirect)
            else router.reload()
        }
    }, [loggedIn])

    useEffect(() => {
        if (loggedOut) router.reload()
    }, [loggedOut])

    if (token == null || token == undefined) {

        return (
            <Container maxWidth="md">
                <Backdrop
                    className={styles.backdrop}
                    open={loggedIn || loggingIn}
                />
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={(values) => handleSubmit(values)}
                >
                    {
                        props => (
                            <Form className={styles.form}>
                                <Field name="email">
                                    {
                                        ({ field, form }) => (
                                            <TextField
                                                fullWidth
                                                name="email"
                                                id="email"
                                                label="Email"
                                                {...field}
                                                helperText={form.touched.email ? form.errors.email : ""}
                                                error={form.touched.email && Boolean(form.errors.email)}
                                            />
                                        )
                                    }

                                </Field>

                                <Field name="password">
                                    {
                                        ({ field, form }) => (
                                            <div className={styles.passwordField}>
                                                <TextField
                                                    fullWidth
                                                    name="password"
                                                    id="password"
                                                    label="Password"
                                                    type={passwordVisible ? "text" : "password"}
                                                    {...field}
                                                    helperText={form.touched.password ? form.errors.password : ""}
                                                    error={form.touched.password && Boolean(form.errors.password)}
                                                />
                                                { form.values?.password?.length > 0 &&
                                                    <IconButton
                                                        className={styles.visibilityButton}
                                                        onClick={togglePasswordVisibility}
                                                    >
                                                        {
                                                            passwordVisible
                                                                ? <VisibilityOff />
                                                                : <Visibility />
                                                        }
                                                    </IconButton>
                                                }
                                            </div>
                                        )
                                    }

                                </Field>

                                <Button
                                    className={loggingIn ? styles.buttonDisabled : styles.button}
                                    variant="contained"
                                    type="submit"
                                    disabled={loggingIn || loggedIn}
                                >
                                    {!loggedIn ? loggingIn ? "Logging in" : "Login" : "Logged in"}
                                    {
                                        loggingIn && <CircularProgress
                                            size={24}
                                            className={styles.buttonProgress}
                                        />
                                    }

                                </Button>
                                <Button
                                    variant="text"
                                    className={styles.forgotPasswordButton}
                                    onClick={() => router.push('/password-reset')}
                                >
                                    Forgot Password?
                                </Button>
                            </Form>
                        )
                    }

                </Formik>

                <Toast
                    open={error != null}
                    onClose={handleClose}
                    severity="error"
                    message={error}
                />
            </Container >
        )
    } else return (
        <Container maxWidth="md" className={styles.alreadyLoggedInContainer}>
            <Backdrop
                className={styles.backdrop}
                open={loggedOut || loggingOut}
            />
            <CheckCircle
                className={styles.checkIcon}
            />
            <Typography
                className={styles.alreadyLoggedInText}
                component="p"
            >
                You're logged in!
            </Typography>
            <Button
                className={loggingOut ? styles.buttonDisabled : styles.logoutButton}
                variant="outlined"
                disabled={loggingOut || loggedOut}
                onClick={() => toggle(true)}
            >
                {!loggedOut ? loggingOut ? "Logging out" : "Logout" : "Logged out"}
                {
                    loggingOut && <CircularProgress
                        size={24}
                        className={styles.buttonProgress}
                    />
                }

            </Button>
            <Toast
                open={error != null}
                onClose={handleClose}
                severity="error"
                message={error}
            />
            <ConfirmationDialog
                title="Logout"
                open={open}
                onClose={() => toggle(false)}
                closeButtonText="Cancel"
                confirmButtonText="Sure"
                confirmAction={handleLogout}
            >
                This will log you out and end your session, you're sure?
            </ConfirmationDialog>
        </Container>
    )
}

export default FirebaseAuth