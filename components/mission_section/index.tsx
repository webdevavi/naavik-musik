import React from 'react'
import TextContentSection from '../text_content_section'

const MissionSection = () => {
    return (
        <TextContentSection
            imageSrc="/mission_section_image.svg"
            headerText="MISSION"
        >
            <p>To bring together all those who share our vision and believe in themselves to stand by it. Anybody can be a NAAVIK, it just takes heart.</p>
        </TextContentSection>
    )
}

export default MissionSection
