import { Fade, Typography } from '@material-ui/core'
import styles from './section_header.module.scss'
import useVisibility from '../../utils/visibility/use_visibility'


interface SectionTypographyProps {
    title: string
}

const SectionTypography: React.FC<SectionTypographyProps> = (
    { title }
) => {
    const { targetRef, enter } = useVisibility()
    return (
        // @ts-ignore
        <div ref={targetRef} className={styles.sectionHeaderContainer}>
            <Fade in={enter} timeout={1000}>
                <Typography
                    variant='h2'
                    component="h2"
                    className={styles.sectionHeader}
                >
                    {title}
                </Typography>
            </Fade>
        </div>
    )
}

export default SectionTypography
