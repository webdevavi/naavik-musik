import { List, ListItem, Slide, SwipeableDrawer } from '@material-ui/core'
import { useRouter } from 'next/router'
import React from 'react'
import { MdClose } from 'react-icons/md'
import useAuth from '../../utils/auth/use_auth'
import MenuButton from '../menu_button'
import SocialIconButtons from '../social_icon_buttons'
import styles from './drawer.module.scss'

interface DrawerProps {
    open: boolean
    toggleDrawer: (open: boolean) => void
}

const Drawer: React.FC<DrawerProps> = ({ open, toggleDrawer }) => {
    const router = useRouter()
    const { token } = useAuth()
    const push = (path: string) => {
        toggleDrawer(false)
        router.push(path)
    }

    const links = [
        {
            path: "/",
            name: "Home"
        },
        {
            path: "/#contact-us",
            name: "Contact Us",
            condition: token == null
        },
        {
            path: "/blogs",
            name: "Blogs"
        },
        {
            path: "/upload-image",
            name: "Images",
            condition: token! + null
        }
    ]

    return (
        <SwipeableDrawer
            anchor="right"
            open={open}
            onClose={() => toggleDrawer(false)}
            onOpen={() => toggleDrawer(true)}
            className={styles.drawer}
        >
            <div className={styles.topbar}>

                <img
                    src='/logo-blue.png'
                    alt="logo"
                    height="60px"
                    width="60px"
                />
                <MenuButton
                    onClick={() => toggleDrawer(false)}
                    Icon={MdClose}
                    color="#E3EBEF"
                />
            </div>
            <nav
                className={styles.nav}
            >
                <List className={styles.list}>
                    {
                        links?.map(
                            ({ path, name, condition }, index) => (condition == undefined || condition) && (
                                <Slide
                                    key={index}
                                    direction="right"
                                    in={open}
                                    timeout={100 * index + 500}

                                >
                                    <ListItem className={styles.button} button onClick={() => push(path)}>
                                        <span className={styles.text}>{name}</span>
                                    </ListItem>
                                </Slide>
                            )
                        )
                    }
                </List>
            </nav>
            <div className={styles.socialIcons}>
                <SocialIconButtons />
            </div>
        </SwipeableDrawer>
    )
}

export default Drawer
