import React from 'react'

const useDrawer = () => {
    const [open, setOpen] = React.useState(false)

    const toggleDrawer = (open: boolean) => setOpen(open)

    return { open, toggleDrawer }
}

export default useDrawer