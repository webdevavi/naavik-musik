import * as admin from 'firebase-admin'

export const verifyIdToken = async (token: string) => {
    const firebasePrivateKey = process.env.FIREBASE_ADMIN_PRIVATE_KEY

    if (!admin.apps.length) {
        admin.initializeApp({
            credential: admin.credential.cert({
                projectId: process.env.FIREBASE_ADMIN_PROJECT_ID,
                clientEmail: process.env.FIREBASE_ADMIN_CLIENT_EMAIL,
                privateKey: firebasePrivateKey.replace(/\\n/g, '\n'),
            }),
            databaseURL: process.env.FIREBASE_ADMIN_DB_URL,
        })
    }

    try {
        return await admin
            .auth()
            .verifyIdToken(token)
    } catch (error) {
        throw error
    }
}
