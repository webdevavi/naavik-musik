const firebaseConfig = {
    apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
    authDomain: "naavik-musik.firebaseapp.com",
    databaseURL: "https://naavik-musik.firebaseio.com",
    projectId: "naavik-musik",
    storageBucket: "naavik-musik.appspot.com",
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: "G-J27LB0EBWK"
}

export default firebaseConfig