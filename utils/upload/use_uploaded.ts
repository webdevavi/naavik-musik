import { useEffect, useState } from "react"
import firebase from 'firebase/app'
import 'firebase/storage'
import initFirebase from '../../firebase'
import { Image } from "../../models/image"

initFirebase()

const useUploaded = () => {
    const [loading, setLoading] = useState(false)
    const [unsafeImagesArray, setUnsafeImagesArray] = useState<Image[]>([])
    const [images, setImages] = useState<Image[]>([])
    const [deleting, setDeleting] = useState(false)
    const [deleted, setDeleted] = useState(false)
    const [error, setError] = useState<string | null>()

    const listRef = firebase.storage()
        .ref("images")

    useEffect(() => {
        setLoading(true)
        listRef.listAll()
            .then(({ items }) => {
                items.forEach(item => {
                    item.getDownloadURL()
                        .then(url =>
                            setUnsafeImagesArray(state => [
                                ...state,
                                new Image({ path: item.fullPath, url, name: item?.name })
                            ])
                        )
                })
                setLoading(false)
            })
            .catch(({ message }) => {
                setError(message)
                setLoading(false)
            })
    }, [])

    useEffect(() => {
        const safeImagesArray = unsafeImagesArray.filter((n, i) => unsafeImagesArray.indexOf(n) === i)
        setImages(safeImagesArray)
    }, [unsafeImagesArray])

    const remove = (image: Image) => {
        setDeleting(true)
        firebase
            .storage()
            .ref(image.path)
            .delete()
            .then(() => {
                setDeleting(false)
                setDeleted(true)
            })
            .catch(({ message }) => {
                setDeleting(false)
                setDeleted(false)
                setError(message)
            })
        return
    }

    const clearNotifs = () => {
        setLoading(false)
        setDeleting(false)
        setDeleted(false)
        setError(null)
    }

    return {
        loading,
        images,
        error,
        remove,
        deleting,
        deleted,
        clearNotifs
    }
}

export default useUploaded