import { useState } from "react"
import firebase from 'firebase/app'
import 'firebase/storage'
import initFirebase from '../../firebase'

initFirebase()

const useUpload = () => {
    const [uploading, setUploading] = useState(false)
    const [uploaded, setUploaded] = useState(false)
    const [error, setError] = useState<string | null>()
    const [currentFileURL, setCurrentFileURL] = useState<string | null>()

    const upload = (files: File[]) => {
        const uploads = []
        setUploading(true)

        files.forEach(file => {
            const url = URL.createObjectURL(file)

            setCurrentFileURL(url)

            const metadata = {
                contentType: file.type
            }

            const uploadTask = firebase
                .storage()
                .ref("images/" + file.name)
                .put(file, metadata)

            uploads.push(uploadTask)
        })

        return Promise
            .all(uploads)
            .then(() => {
                setUploading(false)
                setUploaded(true)
            })
            .catch(({ message }) => {
                setError(message)
                setUploading(false)
            })

    }

    const clearNotifs = () => {
        setUploaded(false)
        setUploading(false)
        setError(null)
        setCurrentFileURL(null)
    }

    return {
        upload,
        uploading,
        uploaded,
        error,
        currentFileURL,
        clearNotifs
    }
}

export default useUpload