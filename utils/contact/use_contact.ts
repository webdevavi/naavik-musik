import { useState } from "react"
import Message from "../../models/message"
import emailjs from 'emailjs-com'

const useContact = () => {
    const [sending, setSending] = useState(false)
    const [sent, setSent] = useState(false)
    const [error, setError] = useState<string | null>()

    const serviceId = process.env.NEXT_PUBLIC_EMAILJS_SERVICE_ID
    const templateId = process.env.NEXT_PUBLIC_EMAILJS_TEMPLATE_ID
    const userId = process.env.NEXT_PUBLIC_EMAILJS_USER_ID

    const send = (message: Message) => {
        setSending(true)
        emailjs.send(
            serviceId,
            templateId,
            message.toJSON(),
            userId
        ).then(() => {
            setSending(false)
            setSent(true)
        }).catch(({ message }) => {
            setSending(false)
            setError(message)
        })
    }

    const clearNotifs = () => {
        setSending(false)
        setSent(false)
        setError(null)
    }

    return {
        send,
        sending,
        sent,
        error,
        clearNotifs
    }
}

export default useContact