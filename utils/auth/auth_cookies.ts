import { parseCookies } from 'nookies'
import { set, remove } from 'js-cookie'

export const getTokenFromCookie = () => parseCookies().auth
export const setTokenToCookie = (token: string) => set("auth", token, { expires: 14 })
export const removeTokenFromCookie = () => remove('auth')