import { useEffect, useState } from 'react'
import initFirebase from '../../firebase'
import firebase from 'firebase/app'
import 'firebase/auth'
import {
    setTokenToCookie,
    getTokenFromCookie,
    removeTokenFromCookie
} from './auth_cookies'
import Axios from 'axios'

initFirebase()

const useAuth = () => {
    const [token, setToken] = useState<string | null>()
    const [loggingIn, setLoggingIn] = useState(false)
    const [loggedIn, setLoggedIn] = useState(false)
    const [loggingOut, setLoggingOut] = useState(false)
    const [loggedOut, setLoggedOut] = useState(false)
    const [error, setError] = useState<string | null>()
    const [emailForPasswordResetSending, setEmailForPasswordResetSending] = useState(false)
    const [emailForPasswordResetSent, setEmailForPasswordResetSent] = useState(false)

    const login = (email: string, password: string) => {
        setLoggingIn(true)
        return firebase.auth().signInWithEmailAndPassword(
            email, password
        )
            .then(async ({ user }) => {
                const token = await user.getIdToken(true)
                setTokenToCookie(token)
                setLoggingIn(false)
                setLoggedIn(true)
                setError(null)
            })
            .catch(({ message }) => {
                removeTokenFromCookie()
                setLoggingIn(false)
                setLoggedIn(false)
                setError(message)
            })

    }

    const logout = () => {
        setLoggingOut(true)
        return firebase
            .auth()
            .signOut()
            .then(() => {
                setLoggedOut(true)
                setLoggedIn(false)
                setLoggingOut(false)
                removeTokenFromCookie()
                setError(null)
            })
            .catch(({ message }) => {
                setError(message)
                setLoggingOut(false)
            })

    }

    const resetPassword = (email: string) => {
        setEmailForPasswordResetSending(true)
        firebase
            .auth()
            .sendPasswordResetEmail(email)
            .then(() => {
                setEmailForPasswordResetSending(false)
                setEmailForPasswordResetSent(true)
            })
            .catch(({ message }) => {
                setEmailForPasswordResetSending(false)
                setError(message)
            })
    }

    const closeNotifs = () => {
        setLoggedOut(false)
        setLoggedIn(false)
        setEmailForPasswordResetSending(false)
        setEmailForPasswordResetSent(false)
        setError(null)
    }

    const validateAndSetToken = async (token: string) => {
        const dev = process.env.NODE_ENV === 'development'
        const server = dev ? 'http://localhost:3000' : process.env.NEXT_PUBLIC_DOMAIN

        try {
            const headers = {
                'Context-Type': 'application/json',
                Authorization: JSON.stringify({ token }),
            };
            const result = await Axios.get(`${server}/api/validate`, { headers }).then((res) => res)
            if (result.data) {
                setTokenToCookie(token)
                setToken(token)
            }
            return result
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        const tokenFromCookie = getTokenFromCookie()
        tokenFromCookie && validateAndSetToken(tokenFromCookie)

        const unsubscribe = firebase.auth().onIdTokenChanged(
            async (user) => {
                const token = await user?.getIdToken(true)
                token && validateAndSetToken(token)
            }
        )

        return () => unsubscribe()
    }, [])

    return {
        token,
        login,
        loggingIn,
        loggedIn,
        logout,
        loggingOut,
        loggedOut,
        resetPassword,
        emailForPasswordResetSending,
        emailForPasswordResetSent,
        error,
        closeNotifs
    }
}

export default useAuth