import copyToClipboard from 'clipboard-copy'
import { useEffect, useState } from 'react'

const useCopy = () => {
    const [copied, setCopied] = useState(false)

    const copy = (text: string) => {
        copyToClipboard(text)
        setCopied(true)
    }

    useEffect(() => {
        if (copied) setTimeout(() => {
            setCopied(false)
        }, 6000);
    }, [copied])

    return { copy, copied }
}

export default useCopy
