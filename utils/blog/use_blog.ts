import { Blog } from '../../models/blog'
import firebase from 'firebase/app'
import 'firebase/firestore'
import initFirebase from '../../firebase'
import { useState } from 'react'
import { v1 } from 'uuid'

initFirebase()

const useBlog = () => {
    const [getting, setGetting] = useState(false)
    const [got, setGot] = useState(false)
    const [blogs, setBlogs] = useState<Blog[] | null>()
    const [blog, setBlog] = useState<Blog | null>()
    const [creating, setCreating] = useState(false)
    const [created, setCreated] = useState(false)
    const [blogId, setBlogId] = useState<string | null>()
    const [removing, setRemoving] = useState(false)
    const [removed, setRemoved] = useState(false)
    const [error, setError] = useState<string | null>()

    const get = (limit: number) => {
        setGetting(true)
        firebase
            .firestore()
            .collection("blogs")
            .orderBy("created_at")
            .limit(limit)
            .get()
            .then((snapshot) => {
                if (!snapshot.empty) {
                    const blogs = snapshot.docs.map(
                        doc => Blog.fromFirebase(
                            doc.data(), doc.id
                        )
                    )
                    setGetting(false)
                    setGot(true)
                    setBlogs(blogs)
                } else {
                    setGetting(false)
                    setError("No recent blogs!")
                }
            })
            .catch(({ message }) => {
                setGetting(false)
                setError(message)
            })
    }

    const getOne = (blogId: string) => {
        setGetting(true)
        firebase
            .firestore()
            .collection("blogs")
            .doc(blogId)
            .get()
            .then((doc) => {
                if (doc.exists) {
                    setGetting(false)
                    setGot(true)
                    const blog = Blog.fromFirebase(doc.data(), doc.id)
                    setBlog(blog)
                } else {
                    setGetting(false)
                    setError("Blog not found")
                }
            })
            .catch(({ message }) => {
                setGetting(false)
                setError(message)
            })
    }

    const create = (blog: Blog) => {
        setCreating(true)
        const blogId = createBlogId(blog?.title)
        setBlogId(blogId)
        console.log(blogId)
        firebase
            .firestore()
            .collection("blogs")
            .doc(blogId)
            .set(blog.toFirebase())
            .then(() => {
                setCreating(false)
                setCreated(true)
            })
            .catch(({ message }) => {
                setCreating(false)
                setError(message)
            })

    }

    const edit = (blog: Blog, blogId: string) => {
        setCreating(true)
        firebase
            .firestore()
            .collection("blogs")
            .doc(blogId)
            .update(blog?.toFirebase(true))
            .then(() => {
                setCreating(false)
                setCreated(true)
            })
            .catch(({ message }) => {
                console.log(message)
                setCreating(false)
                setError(message)
            })
    }

    const remove = (blogId: string) => {
        setRemoving(true)
        firebase
            .firestore()
            .collection("blogs")
            .doc(blogId)
            .delete()
            .then(() => {
                setRemoving(false)
                setRemoved(true)
            })
            .catch(({ message }) => {
                setRemoving(false)
                setError(message)
            })
    }

    const clearNotifs = () => {
        setGetting(false)
        setGot(false)
        setCreating(false)
        setCreated(false)
        setRemoving(false)
        setRemoved(false)
        setError(null)
    }

    return {
        get,
        getOne,
        getting,
        got,
        blogs,
        blog,
        create,
        edit,
        creating,
        created,
        blogId,
        remove,
        removing,
        removed,
        error,
        clearNotifs
    }
}

const createBlogId = (title: string) => {
    const shortString = title
        .slice(0, 99)
        .replaceAll(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')
        .split(" ")
        .join("-")
        .toLowerCase()
    const uniqueString = v1().split("-")[0]
    return shortString + "-" + uniqueString
}

export default useBlog
