import firebase from 'firebase/app'
import 'firebase/firestore'
import initFirebase from '../../firebase'
import { Blog } from '../../models/blog'

export const getBlog = async (blogId: string) => {
    initFirebase()
    const doc = await firebase
        .firestore()
        .collection("blogs")
        .doc(blogId)
        .get()
    if (doc.exists) {
        const blog = Blog.fromFirebase(doc.data(), doc.id)
        return blog
    } else {
        throw Error('Blog not found!')
    }
}

export const getAllBlogs = async () => {
    initFirebase()
    const snapshot = await firebase
        .firestore()
        .collection("blogs")
        .get()
    if (!snapshot.empty) {
        const blogs = snapshot.docs.map(
            doc => Blog.fromFirebase(
                doc.data(), doc.id
            )
        )
        return blogs
    } else {
        throw Error("Blogs not found!")
    }

}