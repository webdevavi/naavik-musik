import { useState, useEffect } from "react"
import { useVisible } from "react-hooks-visible"

const useVisibility = () => {
    const [targetRef, isVisible] = useVisible((vi: number) => vi > 0.5)
    const [enter, setEnter] = useState(false)

    useEffect(() => {
        if (isVisible) setEnter(true)
    }, [isVisible])

    return { targetRef, enter }
}

export default useVisibility