import Axios from "axios";
import qs from 'qs'
import { encode } from 'js-base64'

export interface ISpotify {
    // Access token from Spotify
    accessToken: string
}

export default class Spotify implements ISpotify {
    accessToken: string;

    constructor(accessToken: string) {
        this.accessToken = accessToken
    }

    /**
     * Requests for a temporary access token from Spotify
     * @param {string} clientId - The client id from Spotify Developers account
     * @param {string} clientSecret - The client secret from Spotify Developers account
     */
    public static getAccessToken = async (
        clientId: string,
        clientSecret: string
    ) => {

        // Encoding the Client's Id and Client's secret
        // in a format which Spotify needs
        const base64Token = encode(
            `${clientId}:${clientSecret}`
        )

        // Body and Headers for makin the API request
        const data = qs.stringify({
            'grant_type': 'client_credentials'
        });
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${base64Token}`,
        }

        // Endpoint of the API to make request
        const url = process.env.SPOTIFY_ACCOUNTS_API_ENDPOINT + '/token'

        // Making the request and returning the access token as a string
        return await Axios.post(
            url, data, { headers }
        ).then(res => res?.data?.access_token as string)
    }

    /**
     * Requests for an artist's profile from Spotify
     * @param {string} id - Unique id of the artist from Spotify 
     */
    public getArtist = async (id: string) => {

        // Endpoint of the API to make request
        const url = process.env.SPOTIFY_API_ENDPOINT + '/artists/' + id

        // Headers for makin the API request
        const headers = {
            'Authorization': `Bearer ${this.accessToken}`
        }

        // Making the request and returning the data 
        return await Axios.get(
            url, { headers }
        ).then(res => res?.data)

    }

    /**
     * Requests for an artist's albums from Spotify
     * @param {string} id - Unique id of the artist from Spotify
     */
    public getArtistAlbums = async (id: string) => {

        // Endpoint of the API to make request
        const url = process.env.SPOTIFY_API_ENDPOINT + '/artists/' + id + '/albums'

        // Headers for makin the API request
        const headers = {
            'Authorization': `Bearer ${this.accessToken}`
        }

        // Making the request and returning the data 
        return await Axios.get(
            url, { headers }
        ).then(res => res?.data?.items)
    }

    /**
    * Requests for an artist's top tracks from Spotify for a specific country
    * @param {string} id - Unique id of the artist from Spotify
    * @param {string} country - ISO code of country
    */
    public getArtistTopTracks = async (id: string, country: string) => {

        // Endpoint of the API to make request
        const url = process.env.SPOTIFY_API_ENDPOINT
            + '/artists/'
            + id
            + '/top-tracks'
            + `?country=${country}`

        // Headers for makin the API request
        const headers = {
            'Authorization': `Bearer ${this.accessToken}`
        }

        // Making the request and returning the data 
        return await Axios.get(
            url, { headers }
        ).then(res => res?.data?.tracks)
    }
}